import sys
from PyQt5 import QtWidgets 
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMessageBox
import json
from Login.logIn import *
from tools import cript


class LogInWindow(QtWidgets.QDialog):
    submitted = QtCore.pyqtSignal(str, str,str)

    def __init__(self, parent=None):
        #self.parent = parent
        QtWidgets.QDialog.__init__(self, parent)
        self.parent = parent
        self.ui = Ui_DialogWindow()
        self.ui.setupUi(self)
        self.accessLvl = ""
        self.closeable = False
        
        self.ui.pushButton_6.clicked.connect(self.on_submit)
        self.ui.versionLabel.setText("1.1.0")

    def set_messages(self, username, password):
        self.ui.lineEdit.setText("")
        self.ui.lineEdit_2.setText("")

    
    def closeLog(self):
        self.closeable = True
        self.close()
        self.closeable = False
        
            
    def on_submit(self):
        try:
            with open("users.json" ,"r") as accounts:
                self.users = json.load(accounts)
        except:
            usersList = open("users.json","w+")
            self.users = {"users":[{"username":"admin", "password":"2}p!ao1?u8il>(##-5", "lvl":"admin"}, {"username":"operador","password":"4w!xt!ao4o7a6?9alr1","lvl":"operador"}]}
            json.dump(self.users, usersList, indent = 4)
            usersList.close()

        if self.ui.lineEdit.text() == "" or self.ui.lineEdit_2.text() == "":
                self.ui.label.setText("Username or password is missing")
        else: 
            i = 0
            for user in self.users["users"]:
                
                if user["username"] == self.ui.lineEdit.text():
                    if cript.comparable(user["password"])  == cript.comparable(cript.encrypt(self.ui.lineEdit_2.text())):
                        self.accessLvl = user["lvl"]
                        self.username = self.ui.lineEdit.text()
                        self.password = self.ui.lineEdit_2.text()
                        self.submitted.emit(self.ui.lineEdit.text(),self.ui.lineEdit_2.text(),self.accessLvl)
                        self.ui.label.setText("")
                        self.closeable = True
                        self.ui.lineEdit.setText("")
                        self.ui.lineEdit_2.setText("")
                        self.close()
                    else:
                        self.ui.label.setText("Wrong password")
                else:
                    i+=1
            if len(self.users)+1 == i:
                self.ui.label.setText("Invalid user")