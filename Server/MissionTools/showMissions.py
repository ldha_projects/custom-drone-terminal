import sys
import json
from PyQt5 import QtWidgets 
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget,  QInputDialog, QDialog, QLineEdit, QMessageBox
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QTimer
import numpy as np
import time
from MissionTools.missionsList import *


import MissionTools.createMission
import sys



class ShowMissions(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.parent = parent
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.actualMissions = []
        self.selectedItem = 0

        
       
        #Open json, append missions to list
        try:
            with open("missions.json" ,"r") as f:
                self.data = json.load(f)
            f.close()
        except:
            f = open("missions.json","w+")
            self.data = {}
            json.dump(self.data, f, indent = 4)
            f.close()
        self.missions=[]
        for x in self.data:
            self.missions.append(x)
        for mission in self.missions:
            self.ui.listWidget.addItem(mission)


        self.ui.delete_2.clicked.connect(self.delete) #Delete button
        #self.ui.edit.clicked.connect(self.edit)


    def updateList(self):
        self.ui.listWidget.clear()
        with open("missions.json" ,"r") as f:
            self.data = json.load(f)
        f.close()
        missions=[]
        for x in self.data:
            missions.append(x)
        for mission in missions:
            self.ui.listWidget.addItem(mission)
            
    
    #Delete Mission
    def delete(self):     
        for mission in self.missions:
            selec = self.ui.listWidget.currentRow()
            if mission == self.missions[selec]:
                self.missions.remove(self.missions[selec])
                del self.data[mission]
        with open('missions.json', 'w') as file:
            json.dump(self.data, file)
        self.updateList()



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QDialog()
    ui = ShowMissions()
    ui.show()
    sys.exit(app.exec_())