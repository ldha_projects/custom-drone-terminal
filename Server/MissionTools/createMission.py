import sys
import json
from PyQt5 import QtWidgets 
from PyQt5 import QtGui
from PyQt5 import QtCore 
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget,  QInputDialog, QDialog, QLineEdit, QMessageBox, QTableWidget,QTableWidgetItem
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QTimer, QSize
import numpy as np
import time
from MissionTools.addMission import *
import random
import MissionTools.showMissions



class MissionCreatorDrone(QtWidgets.QDialog):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        #Message
        #self.ui.label_9.setText(self.username)

        #open missionsDrone Json
        

        try:
            with open("missions.json", "r") as missions:
                self.data = json.load(missions)
        except:
            f = open("missions.json","w+")
            self.data = {}
            json.dump(self.data, f, indent = 4)
            f.close() 
            
        self.missions=[]
        for x in self.data:
            self.missions.append(x)
            
        #Add instruction buttons
        self.ui.ond.clicked.connect(self.newondInstruction)
        self.ui.ofd.clicked.connect(self.newofdInstruction)
        self.ui.rth.clicked.connect(self.newrthInstruction)
        self.ui.stn.clicked.connect(self.newstnInstruction)
        self.ui.tlt.clicked.connect(self.newtltInstruction)
        self.ui.trt.clicked.connect(self.newtrtInstruction)
        self.ui.gup.clicked.connect(self.newgupInstruction)
        self.ui.gdn.clicked.connect(self.newgdnInstruction)
        self.ui.onc.clicked.connect(self.newoncInstruction)
        self.ui.ofc.clicked.connect(self.newofcInstruction)
        self.ui.str.clicked.connect(self.newstrInstruction)
        self.ui.psa.clicked.connect(self.newpsaInstruction)
        self.ui.pht.clicked.connect(self.newphtInstruction)
        self.ui.end.clicked.connect(self.newendInstruction)
        self.ui.save.clicked.connect(self.saveInstructions)
        #self.ui.dlt.clicked.connect(self.edit_messages)
        self.ui.dlt.clicked.connect(self.dltInstruction)
        
        #Edit/Delete mission button
        self.ui.edtMission.clicked.connect(self.editMission)
        self.ui.dltMiss.clicked.connect(showMissions.ShowMissions(self).show)  
        

    def newondInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("ond"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))       
    def newofdInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("ofd"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newrthInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("rth"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newstnInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("stn"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newtltInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("tlt"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newtrtInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("trt"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newgupInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("gup"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newgdnInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("gdn"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newoncInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("onc"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newofcInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("ofc"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newstrInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("str"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newpsaInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("psa"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newphtInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("pht"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newendInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("end"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def newedtInstruction(self):
        content, ok = QInputDialog.getText(self, 'Durarion in seconds', 'Instruction duration in seconds')
        if ok and content is not None:
            self.num = self.ui.missionsTable.rowCount()
            self.ui.missionsTable.setRowCount(self.num+1)
            self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem("edt"))
            self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(content))
    def dltInstruction(self):
        row = self.ui.missionsTable.currentRow()
        reply = QMessageBox.question(self, "Erase Instruction", "Do you want to wrase instruction number " + str(row+1) +"?", QMessageBox.Yes|QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.ui.missionsTable.removeRow(row)
    
    def saveInstructions(self):
        self.num = self.ui.missionsTable.rowCount()
        with open('missions.json','r') as f:
            self.data=json.load(f)
        f.close()
        missions=[]
        for x in self.data:
            missions.append(x)
        loop = 0
        if self.ui.missionName.text() == "":
            self.ui.missingName.setText("Name is Missing")      
        else:
            self.ui.missingName.setText("")
            if self.ui.missionName.text() in missions:
                #self.ui.missingName.setText("Name Used")
                #agregar variable que indique si se esta editando o el nombre coincide con el ya existente
                response = QMessageBox.question(self, "Save changes","The mission has been modified, \n do you like to save changes?", QMessageBox.Yes|QMessageBox.No)
                if response == QMessageBox.Yes:
                    self.data[self.ui.missionName.text()] = []
                    while loop < self.num:
                        self.data[self.ui.missionName.text()].append([self.ui.missionsTable.item(loop,0).text(),int(self.ui.missionsTable.item(loop,1).text())])
                        loop += 1
                    with open("missions.json" ,"w") as f:
                        json.dump(self.data, f)
                    f.close()
                    self.ui.missionsTable.setRowCount(0)
                    self.ui.missionName.setText("")
                    #Devolver la variable a un estado de no editando        
               
            else:
                self.data[self.ui.missionName.text()] = []
                while loop < self.num:
                    self.data[self.ui.missionName.text()].append([self.ui.missionsTable.item(loop,0).text(),int(self.ui.missionsTable.item(loop,1).text())])
                    loop += 1
                with open("missions.json" ,"w") as f:
                    json.dump(self.data, f)
                f.close()
                self.ui.missionsTable.setRowCount(0)
                self.ui.missionName.setText("")
 


    def editMission(self):
        with open('missions.json','r') as f:
            self.data=json.load(f)
        missions=[]
        for x in self.data:
            missions.append(x)
            print(x)
        
        
        item, ok = QInputDialog.getItem(self,"Edit Mission","Choose mission:", missions, 0, False)
        
        
        if ok:
            print(item)
            self.ui.missionName.setText(item)
            self.ui.missionsTable.setRowCount(0)
            for items in range(0,len(self.data[item])):
                self.num = self.ui.missionsTable.rowCount()
                self.ui.missionsTable.setRowCount(self.num+1)
                self.ui.missionsTable.setItem(self.num,0, QTableWidgetItem(self.data[item][items][0]))
                self.ui.missionsTable.setItem(self.num,1, QTableWidgetItem(str(self.data[item][items][1])))
                print(self.data[item][items][1])
            
                

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = MissionCreatorDrone()
    ui.show()
    sys.exit(app.exec_())