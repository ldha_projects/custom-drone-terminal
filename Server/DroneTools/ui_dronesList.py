# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui_dronesList.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_dronesList(object):
    def setupUi(self, dronesList):
        dronesList.setObjectName("dronesList")
        dronesList.resize(283, 568)
        dronesList.setStyleSheet("background-color: rgb(0, 0, 0);")
        self.verticalLayout = QtWidgets.QVBoxLayout(dronesList)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(dronesList)
        self.label.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 11pt \"Microsoft YaHei\";\n"
"\n"
"")
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget(dronesList)
        self.listWidget.setStyleSheet("QListWidget{\n"
"font: 12pt \"Century Gothic\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight: bold;\n"
"text-align: center;\n"
"}\n"
"\n"
"QListWidget::item::hover {\n"
"    color:rgb(255, 255, 255);\n"
"    background-color:rgb(125, 125, 125)\n"
"}\n"
"\n"
"")
        self.listWidget.setObjectName("listWidget")
        self.verticalLayout.addWidget(self.listWidget, 0, QtCore.Qt.AlignHCenter)
        self.deleteButton = QtWidgets.QPushButton(dronesList)
        self.deleteButton.setStyleSheet("QPushButton{\n"
"color:rgb(255, 255, 255);\n"
"font: 75 15pt \"Microsoft YaHei\";\n"
"}\n"
"QPushButton:hover{\n"
"border: 1px solid rgb(255, 255, 255);\n"
"}\n"
"\n"
"\n"
"")
        self.deleteButton.setObjectName("deleteButton")
        self.verticalLayout.addWidget(self.deleteButton)
        self.label.raise_()
        self.deleteButton.raise_()
        self.listWidget.raise_()

        self.retranslateUi(dronesList)
        QtCore.QMetaObject.connectSlotsByName(dronesList)

    def retranslateUi(self, dronesList):
        _translate = QtCore.QCoreApplication.translate
        dronesList.setWindowTitle(_translate("dronesList", "Delete Drones"))
        self.label.setText(_translate("dronesList", "Drones"))
        self.listWidget.setToolTip(_translate("dronesList", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.listWidget.setWhatsThis(_translate("dronesList", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.deleteButton.setText(_translate("dronesList", "Delete"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    dronesList = QtWidgets.QDialog()
    ui = Ui_dronesList()
    ui.setupUi(dronesList)
    dronesList.show()
    sys.exit(app.exec_())
