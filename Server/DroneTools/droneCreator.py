import sys
import json
from PyQt5 import QtWidgets 
from PyQt5 import QtGui
from PyQt5 import QtCore 
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget,  QInputDialog, QDialog, QLineEdit, QMessageBox, QTableWidget,QTableWidgetItem
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QTimer, QSize
import numpy as np
import time
from DroneTools.ui_droneCreator import *
import random
import DroneTools.showDrones
from ipaddress import ip_address

def realIP(alledIP):
    if len(alledIP.split('.'))==4:
        y=alledIP.split('.')
        for z in y:
            if int(z) in range(255):
                return True
    return False

class CreateDrone(QtWidgets.QDialog):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.ui = Ui_DroneCreator()
        self.ui.setupUi(self)
        self.droneData = {}

        #open settings Json
        try:
            with open("settings.json", "r") as settingsData:
                self.data = json.load(settingsData)
                settingsData.close()            
        except:
            settingsData = open("settings.json","w+")
            self.data = {"droneParameters" : {"adjustVelocity": [0,"Fast"], "imgFormatDunas": [0,"JPEG"],"videoResDunas": [0, "HD"],"saveimgDunas": "C:\\"}, "drones":{}}
            json.dump(self.data, settingsData, indent = 4)
            settingsData.close() 
            
            

        #Buttons
        self.ui.saveButton.clicked.connect(self.saveDrone)
        self.ui.editButton.clicked.connect(self.editMission) 
        self.ui.deleteButton.clicked.connect(showDrones.ShowDrones(self).show)
    
    #Functions
    def save(self):
        with open("settings.json", "r") as settingsData:
            self.data = json.load(settingsData)
            settingsData.close()
        self.data["drones"][self.ui.nameEdit.text()] = {"Name":self.ui.nameEdit.text(), "IP":self.ui.ipEdit.text(),"Description":self.ui.descriptionEdit.toPlainText()}
        with open('settings.json', 'w') as file:
            json.dump(self.data, file, indent = 4)
        self.ui.nameEdit.setText("")
        self.ui.ipEdit.setText("")
        self.ui.descriptionEdit.setText("")


    def saveDrone(self):
        with open("settings.json", "r") as settingsData:
            self.data = json.load(settingsData)
            settingsData.close()
        if self.ui.nameEdit.text() == "" or self.ui.ipEdit.text() == "" or not realIP(self.ui.ipEdit.text()):
            QMessageBox.information(self, "Can't save changes", "Name and a valid IP are required",QMessageBox.Ok)
        else:
            if self.ui.nameEdit.text() in self.data["drones"]:
                reply = QMessageBox.question(self, "Save drone", "Do you want to update "+self.ui.nameEdit.text()+"?", QMessageBox.Yes|QMessageBox.No)
                if reply == QMessageBox.Yes:
                    self.save()
            else:
                reply = QMessageBox.question(self, "Save drone", "Do you want to save new drone ?", QMessageBox.Yes|QMessageBox.No)
                if reply == QMessageBox.Yes:
                    self.save()

                
            

    def editMission(self):
        with open("settings.json", "r") as settingsData:
            self.data = json.load(settingsData)
            settingsData.close()        
        item, ok = QInputDialog.getItem(self,"Edit Drone","Choose drone:", self.data["drones"], 0, False)
        if ok:
            self.ui.nameEdit.setText(item)
            self.ui.ipEdit.setText(self.data["drones"][item]["IP"])
            self.ui.descriptionEdit.setText(self.data["drones"][item]["Description"])         

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = CreateDrone()
    ui.show()
    sys.exit(app.exec_())