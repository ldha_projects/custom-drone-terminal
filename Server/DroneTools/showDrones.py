import sys
import json
from PyQt5 import QtWidgets 
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget,  QInputDialog, QDialog, QLineEdit, QMessageBox
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QTimer
import numpy as np
import time
from DroneTools.ui_dronesList import *
#import createDrone
import sys


class ShowDrones(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.parent = parent
        self.ui = Ui_dronesList()
        self.ui.setupUi(self)
        self.actualMissions = []
        self.selectedItem = 0
        self.droneData = {}
        self.data = []

        #Open json
        try:
            with open("settings.json", "r") as settingsData:
                self.data = json.load(settingsData)
                settingsData.close()  
            for drone in self.data["drones"]:
                self.ui.listWidget.addItem(drone)
        except:
            settingsData = open("settings.json","w+")
            self.data = {"droneParameters" : {"adjustVelocity": [0,"Fast"], "imgFormatDunas": [0,"JPEG"],"videoResDunas": [0, "HD"],"saveimgDunas": "C:\\"}, "drones":{}}
            json.dump(self.data, settingsData, indent = 4)
            settingsData.close()
            for drone in self.data["drones"]:
                self.ui.listWidget.addItem(drone)


        #Delete button
        self.ui.deleteButton.clicked.connect(self.delete) 

    #Update drone's list
    def updateList(self):
        self.ui.listWidget.clear()
        with open("settings.json", "r") as settingsData:
            self.data = json.load(settingsData)
            settingsData.close()  
        for drone in self.data["drones"]:
            self.ui.listWidget.addItem(drone)
    
    #Delete Drone
    def delete(self):     
        selec = self.ui.listWidget.currentItem().text()
        del self.data["drones"][selec]
        with open('settings.json', 'w') as file:
            json.dump(self.data, file, indent = 4)
        self.updateList()
        
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QDialog()
    ui = ShowDrones()
    ui.show()
    sys.exit(app.exec_())