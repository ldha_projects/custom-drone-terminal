# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui_droneCreator.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_DroneCreator(object):
    def setupUi(self, DroneCreator):
        DroneCreator.setObjectName("DroneCreator")
        DroneCreator.resize(348, 289)
        DroneCreator.setMinimumSize(QtCore.QSize(348, 289))
        DroneCreator.setMaximumSize(QtCore.QSize(348, 289))
        DroneCreator.setStyleSheet("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255)")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(DroneCreator)
        self.verticalLayout_6.setContentsMargins(-1, 9, -1, -1)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.pushButton = QtWidgets.QPushButton(DroneCreator)
        self.pushButton.setStyleSheet("background-color:rgb(63, 63, 63);\n"
"border-radius: 0px;")
        self.pushButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\icons/DunasTech.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton.setIcon(icon)
        self.pushButton.setIconSize(QtCore.QSize(160, 45))
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_5.addWidget(self.pushButton)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.line_2 = QtWidgets.QFrame(DroneCreator)
        self.line_2.setStyleSheet("background-color:rgb(63, 63, 63);")
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout_4.addWidget(self.line_2)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.nameLabel = QtWidgets.QLabel(DroneCreator)
        self.nameLabel.setObjectName("nameLabel")
        self.horizontalLayout_4.addWidget(self.nameLabel)
        spacerItem1 = QtWidgets.QSpacerItem(5, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem1)
        self.nameEdit = QtWidgets.QLineEdit(DroneCreator)
        self.nameEdit.setStyleSheet("background-color: rgb(58, 58, 58);\n"
"border-radius:5px;")
        self.nameEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.nameEdit.setPlaceholderText("")
        self.nameEdit.setObjectName("nameEdit")
        self.horizontalLayout_4.addWidget(self.nameEdit)
        self.verticalLayout_4.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.ipLabel = QtWidgets.QLabel(DroneCreator)
        self.ipLabel.setObjectName("ipLabel")
        self.horizontalLayout_5.addWidget(self.ipLabel)
        spacerItem2 = QtWidgets.QSpacerItem(22, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem2)
        self.ipEdit = QtWidgets.QLineEdit(DroneCreator)
        self.ipEdit.setStyleSheet("background-color: rgb(58, 58, 58);\n"
"border-radius:5px;")
        self.ipEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.ipEdit.setObjectName("ipEdit")
        self.horizontalLayout_5.addWidget(self.ipEdit)
        self.verticalLayout_4.addLayout(self.horizontalLayout_5)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.line = QtWidgets.QFrame(DroneCreator)
        self.line.setMinimumSize(QtCore.QSize(0, 1))
        self.line.setStyleSheet("background-color:rgb(63, 63, 63);")
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_3.addWidget(self.line)
        self.descriptionLabel = QtWidgets.QLabel(DroneCreator)
        self.descriptionLabel.setObjectName("descriptionLabel")
        self.verticalLayout_3.addWidget(self.descriptionLabel)
        self.descriptionEdit = QtWidgets.QTextEdit(DroneCreator)
        self.descriptionEdit.setStyleSheet("background-color: rgb(58, 58, 58);\n"
"border-radius:10px;")
        self.descriptionEdit.setObjectName("descriptionEdit")
        self.verticalLayout_3.addWidget(self.descriptionEdit)
        self.verticalLayout_4.addLayout(self.verticalLayout_3)
        self.line_3 = QtWidgets.QFrame(DroneCreator)
        self.line_3.setStyleSheet("background-color:rgb(63, 63, 63);")
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout_4.addWidget(self.line_3)
        self.horizontalLayout_3.addLayout(self.verticalLayout_4)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem3)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem4)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.deleteButton = QtWidgets.QPushButton(DroneCreator)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.deleteButton.sizePolicy().hasHeightForWidth())
        self.deleteButton.setSizePolicy(sizePolicy)
        self.deleteButton.setMinimumSize(QtCore.QSize(100, 0))
        self.deleteButton.setStyleSheet("QPushButton{\n"
"    color: rgb(255, 255, 255);\n"
"    font: 75 10pt \"Microsoft YaHei\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"    background-color: rgba(132, 132, 132,0.3);\n"
"}")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(".\\icons/createMissions/delete.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.deleteButton.setIcon(icon1)
        self.deleteButton.setObjectName("deleteButton")
        self.horizontalLayout.addWidget(self.deleteButton)
        self.editButton = QtWidgets.QPushButton(DroneCreator)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.editButton.sizePolicy().hasHeightForWidth())
        self.editButton.setSizePolicy(sizePolicy)
        self.editButton.setMinimumSize(QtCore.QSize(100, 0))
        self.editButton.setStatusTip("")
        self.editButton.setWhatsThis("")
        self.editButton.setStyleSheet("QPushButton{\n"
"    color: rgb(255, 255, 255);\n"
"    font: 75 10pt \"Microsoft YaHei\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"    background-color: rgba(132, 132, 132,0.3);\n"
"}")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(".\\icons/createMissions/edit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.editButton.setIcon(icon2)
        self.editButton.setObjectName("editButton")
        self.horizontalLayout.addWidget(self.editButton)
        self.saveButton = QtWidgets.QPushButton(DroneCreator)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.saveButton.sizePolicy().hasHeightForWidth())
        self.saveButton.setSizePolicy(sizePolicy)
        self.saveButton.setMinimumSize(QtCore.QSize(100, 0))
        self.saveButton.setStyleSheet("QPushButton{\n"
"    color: rgb(255, 255, 255);\n"
"    font: 75 10pt \"Microsoft YaHei\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"    background-color: rgba(132, 132, 132,0.3);\n"
"}")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(".\\icons/createMissions/save.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.saveButton.setIcon(icon3)
        self.saveButton.setIconSize(QtCore.QSize(25, 25))
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.horizontalLayout_2.addLayout(self.horizontalLayout)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem5)
        self.verticalLayout_5.addLayout(self.horizontalLayout_2)
        self.verticalLayout_6.addLayout(self.verticalLayout_5)

        self.retranslateUi(DroneCreator)
        QtCore.QMetaObject.connectSlotsByName(DroneCreator)

    def retranslateUi(self, DroneCreator):
        _translate = QtCore.QCoreApplication.translate
        DroneCreator.setWindowTitle(_translate("DroneCreator", "Drone Creation"))
        self.nameLabel.setText(_translate("DroneCreator", "Name:"))
        self.ipLabel.setText(_translate("DroneCreator", "IP:"))
        self.ipEdit.setPlaceholderText(_translate("DroneCreator", "0.0.0.0"))
        self.descriptionLabel.setText(_translate("DroneCreator", "Description"))
        self.deleteButton.setText(_translate("DroneCreator", "Delete"))
        self.editButton.setToolTip(_translate("DroneCreator", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.editButton.setText(_translate("DroneCreator", "Edit    "))
        self.saveButton.setText(_translate("DroneCreator", "Save"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    DroneCreator = QtWidgets.QWidget()
    ui = Ui_DroneCreator()
    ui.setupUi(DroneCreator)
    DroneCreator.show()
    sys.exit(app.exec_())
