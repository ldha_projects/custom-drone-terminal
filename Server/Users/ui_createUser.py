# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui_createUser.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_createUser(object):
    def setupUi(self, createUser):
        createUser.setObjectName("createUser")
        createUser.resize(495, 442)
        createUser.setMaximumSize(QtCore.QSize(495, 442))
        createUser.setStyleSheet("background-color: rgb(53, 53, 53);\n"
"color:white;")
        self.verticalLayout = QtWidgets.QVBoxLayout(createUser)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.titleLabel = QtWidgets.QLabel(createUser)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.titleLabel.sizePolicy().hasHeightForWidth())
        self.titleLabel.setSizePolicy(sizePolicy)
        self.titleLabel.setStyleSheet("color:rgb(255, 255, 255);\n"
"font: 75 18pt \"Microsoft YaHei\";")
        self.titleLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.titleLabel.setObjectName("titleLabel")
        self.horizontalLayout_2.addWidget(self.titleLabel)
        self.viewUsersButton = QtWidgets.QPushButton(createUser)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.viewUsersButton.sizePolicy().hasHeightForWidth())
        self.viewUsersButton.setSizePolicy(sizePolicy)
        self.viewUsersButton.setStyleSheet("QPushButton{\n"
"    background-color:transparent;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: rgb(58, 58, 58);\n"
"    border-radius: 5px;\n"
"}\n"
"")
        self.viewUsersButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\icons/user.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.viewUsersButton.setIcon(icon)
        self.viewUsersButton.setObjectName("viewUsersButton")
        self.horizontalLayout_2.addWidget(self.viewUsersButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.line_2 = QtWidgets.QFrame(createUser)
        self.line_2.setStyleSheet("background-color:rgb(63, 63, 63);")
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout_4.addWidget(self.line_2)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.nameLabel = QtWidgets.QLabel(createUser)
        self.nameLabel.setMaximumSize(QtCore.QSize(16777215, 50))
        self.nameLabel.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.nameLabel.setObjectName("nameLabel")
        self.horizontalLayout_4.addWidget(self.nameLabel)
        self.nameEdit = QtWidgets.QLineEdit(createUser)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.nameEdit.sizePolicy().hasHeightForWidth())
        self.nameEdit.setSizePolicy(sizePolicy)
        self.nameEdit.setMaximumSize(QtCore.QSize(350, 50))
        self.nameEdit.setStyleSheet("background-color: rgb(58, 58, 58);\n"
"border-radius:5px;")
        self.nameEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.nameEdit.setPlaceholderText("")
        self.nameEdit.setObjectName("nameEdit")
        self.horizontalLayout_4.addWidget(self.nameEdit)
        self.verticalLayout_4.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.ipLabel = QtWidgets.QLabel(createUser)
        self.ipLabel.setMaximumSize(QtCore.QSize(16777215, 50))
        self.ipLabel.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.ipLabel.setObjectName("ipLabel")
        self.horizontalLayout_5.addWidget(self.ipLabel)
        self.passwordEdit = QtWidgets.QLineEdit(createUser)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.passwordEdit.sizePolicy().hasHeightForWidth())
        self.passwordEdit.setSizePolicy(sizePolicy)
        self.passwordEdit.setMaximumSize(QtCore.QSize(350, 50))
        self.passwordEdit.setStyleSheet("background-color: rgb(58, 58, 58);\n"
"border-radius:5px;")
        self.passwordEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.passwordEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.passwordEdit.setPlaceholderText("")
        self.passwordEdit.setObjectName("passwordEdit")
        self.horizontalLayout_5.addWidget(self.passwordEdit)
        self.verticalLayout_4.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.ipLabel_2 = QtWidgets.QLabel(createUser)
        self.ipLabel_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.ipLabel_2.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";")
        self.ipLabel_2.setObjectName("ipLabel_2")
        self.horizontalLayout_6.addWidget(self.ipLabel_2)
        self.levelComboBox = QtWidgets.QComboBox(createUser)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.levelComboBox.sizePolicy().hasHeightForWidth())
        self.levelComboBox.setSizePolicy(sizePolicy)
        self.levelComboBox.setMinimumSize(QtCore.QSize(0, 0))
        self.levelComboBox.setMaximumSize(QtCore.QSize(350, 50))
        self.levelComboBox.setStyleSheet("QComboBox{\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    \n"
"    padding-left:75px;\n"
"background-color: rgb(58, 58, 58);\n"
"border-radius:5px;\n"
"}\n"
"\n"
"\n"
"QListView{\n"
"    border: none;\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(50, 50, 50);\n"
"    font-weight: bold;\n"
"    selection-background-color: rgb(45, 45, 45);\n"
"}\n"
"")
        self.levelComboBox.setObjectName("levelComboBox")
        self.levelComboBox.addItem("")
        self.levelComboBox.addItem("")
        self.horizontalLayout_6.addWidget(self.levelComboBox)
        self.verticalLayout_4.addLayout(self.horizontalLayout_6)
        self.line_3 = QtWidgets.QFrame(createUser)
        self.line_3.setStyleSheet("background-color:rgb(63, 63, 63);")
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout_4.addWidget(self.line_3)
        self.horizontalLayout_3.addLayout(self.verticalLayout_4)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.verticalLayout.addLayout(self.verticalLayout_5)
        self.horizontalGroupBox = QtWidgets.QGroupBox(createUser)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.horizontalGroupBox.sizePolicy().hasHeightForWidth())
        self.horizontalGroupBox.setSizePolicy(sizePolicy)
        self.horizontalGroupBox.setStyleSheet("border:none;")
        self.horizontalGroupBox.setObjectName("horizontalGroupBox")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalGroupBox)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.deleteButton = QtWidgets.QPushButton(self.horizontalGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.deleteButton.sizePolicy().hasHeightForWidth())
        self.deleteButton.setSizePolicy(sizePolicy)
        self.deleteButton.setMinimumSize(QtCore.QSize(100, 0))
        self.deleteButton.setMaximumSize(QtCore.QSize(16777215, 65))
        self.deleteButton.setStyleSheet("QPushButton{\n"
"    color: rgb(255, 255, 255);\n"
"    font: 75 10pt \"Microsoft YaHei\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"    background-color: rgba(132, 132, 132,0.3);\n"
"}")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(".\\icons/createMissions/delete.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.deleteButton.setIcon(icon1)
        self.deleteButton.setObjectName("deleteButton")
        self.horizontalLayout.addWidget(self.deleteButton)
        self.saveButton = QtWidgets.QPushButton(self.horizontalGroupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.saveButton.sizePolicy().hasHeightForWidth())
        self.saveButton.setSizePolicy(sizePolicy)
        self.saveButton.setMinimumSize(QtCore.QSize(100, 0))
        self.saveButton.setMaximumSize(QtCore.QSize(16777215, 65))
        self.saveButton.setStyleSheet("QPushButton{\n"
"    color: rgb(255, 255, 255);\n"
"    font: 75 10pt \"Microsoft YaHei\";\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"    background-color: rgba(132, 132, 132,0.3);\n"
"}")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(".\\icons/createMissions/save.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.saveButton.setIcon(icon2)
        self.saveButton.setIconSize(QtCore.QSize(25, 25))
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.verticalLayout.addWidget(self.horizontalGroupBox)

        self.retranslateUi(createUser)
        QtCore.QMetaObject.connectSlotsByName(createUser)

    def retranslateUi(self, createUser):
        _translate = QtCore.QCoreApplication.translate
        createUser.setWindowTitle(_translate("createUser", "Form"))
        self.titleLabel.setText(_translate("createUser", "New User"))
        self.nameLabel.setText(_translate("createUser", "Username"))
        self.ipLabel.setText(_translate("createUser", "Password"))
        self.ipLabel_2.setText(_translate("createUser", "Level"))
        self.levelComboBox.setItemText(0, _translate("createUser", "admin"))
        self.levelComboBox.setItemText(1, _translate("createUser", "operador"))
        self.deleteButton.setText(_translate("createUser", "Delete"))
        self.saveButton.setText(_translate("createUser", "Save"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    createUser = QtWidgets.QWidget()
    ui = Ui_createUser()
    ui.setupUi(createUser)
    createUser.show()
    sys.exit(app.exec_())
