import sys
import json
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget,  QInputDialog, QDialog, QLineEdit, QMessageBox
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QTimer
import numpy as np
import time
from Users.ui_viewUsers import *
import sys


class ViewUsers(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.parent = parent
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.openJson()

    def openJson(self):
        with open("./users.json", "r") as usersData:
            self.data = json.load(usersData)
            usersData.close()
        for user in self.data["users"]:
            self.ui.listWidget.addItem(user['username'])


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QDialog()
    ui = ViewUsers()
    ui.show()
    sys.exit(app.exec_())
