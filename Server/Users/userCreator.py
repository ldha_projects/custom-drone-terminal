import sys
import json
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget,  QInputDialog, QDialog, QLineEdit, QMessageBox, QTableWidget, QTableWidgetItem
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QTimer, QSize
import numpy as np
import time
from Users.ui_createUser import *
from tools import cript
from Users.usersList import ShowUsers
from Users.viewUsers import ViewUsers


class UserCreator(QtWidgets.QDialog):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.ui = Ui_createUser()
        self.ui.setupUi(self)
        self.openJson()
        self.exist = False
        self.userType = None
        # Buttons
        self.ui.saveButton.clicked.connect(self.saveUsers)
        self.ui.deleteButton.clicked.connect(ShowUsers(self).show)
        self.ui.viewUsersButton.clicked.connect(ViewUsers(self).show)

    def openJson(self):
        with open("./users.json", "r") as usersData:
            self.data = json.load(usersData)
            usersData.close()

    def updateJson(self):
        with open('./users.json', 'w') as file:
            json.dump(self.data, file, indent=4)
        self.ui.nameEdit.setText("")
        self.ui.passwordEdit.setText("")

    def saveUsers(self):
        self.openJson()
        if self.ui.nameEdit.text() == "" or self.ui.passwordEdit.text() == "":
            QMessageBox.information(
                self, "Can't save changes", "Name and password are required", QMessageBox.Ok)
        else:
            for i in self.data["users"]:
                if i['username'] == self.ui.nameEdit.text():
                    self.userType = i['lvl']
                    self.exist = True
            if self.exist == True and self.userType == 'operador':
                reply = QMessageBox.question(
                    self, "Update", "Do you want to update this user ?", QMessageBox.Yes | QMessageBox.No)
                if reply == QMessageBox.Yes:
                    self.update()
                    self.exist = False
            elif self.exist == True and self.userType == 'admin':
                QMessageBox.information(
                    self, "Invalid", "You can't update an administrator", QMessageBox.Ok)
                self.exist = False
            else:
                reply = QMessageBox.question(
                    self, "Save drone", "Do you want to save new user ?", QMessageBox.Yes | QMessageBox.No)
                if reply == QMessageBox.Yes:
                    self.save()

    def save(self):
        self.openJson()
        self.data['users'].append({"username": self.ui.nameEdit.text(), "password": cript.encrypt(
            self.ui.passwordEdit.text()), "lvl": self.ui.levelComboBox.currentText()})
        self.updateJson()

    def update(self):
        self.openJson()
        a = 0
        for i in self.data['users']:
            if self.ui.nameEdit.text() in i['username']:
                self.data['users'][a]['password'] = cript.encrypt(
                    self.ui.passwordEdit.text())
                self.data['users'][a]['lvl'] = self.ui.levelComboBox.currentText()
            a += 1
        self.updateJson()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QDialog()
    ui = UserCreator()
    ui.show()
    sys.exit(app.exec_())
