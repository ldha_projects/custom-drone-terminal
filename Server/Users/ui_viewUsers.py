# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui_viewUsers.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(283, 568)
        Dialog.setStyleSheet("background-color: rgb(0, 0, 0);")
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 11pt \"Microsoft YaHei\";\n"
"\n"
"")
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.listWidget = QtWidgets.QListWidget(Dialog)
        self.listWidget.setStyleSheet("QListWidget{\n"
"font: 12pt \"Century Gothic\";\n"
"color: rgb(255, 255, 255);\n"
"font-weight: bold;\n"
"text-align: center;\n"
"}\n"
"\n"
"QListWidget::item::hover {\n"
"    color:rgb(255, 255, 255);\n"
"    background-color:rgb(125, 125, 125)\n"
"}\n"
"\n"
"")
        self.listWidget.setObjectName("listWidget")
        self.verticalLayout.addWidget(self.listWidget, 0, QtCore.Qt.AlignHCenter)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "View users"))
        self.label.setText(_translate("Dialog", "Users"))
        self.listWidget.setToolTip(_translate("Dialog", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))
        self.listWidget.setWhatsThis(_translate("Dialog", "<html><head/><body><p align=\"center\"><br/></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
