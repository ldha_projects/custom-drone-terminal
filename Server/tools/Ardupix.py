import sys
import numpy as np
import socket
import threading
from time import sleep
from PrimalSockets import SenderSkt


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


class ArduPix4():
    def __init__(self, ipaddr):
        self.connected = False
        self.comm = None
        self.telemetry = ''
        self.sendInstruction = SenderSkt(host=get_ip_address(), port=9999)

    # Drone
    def connectDrone(self):
        try:
            ans = self.sendInstruction.send(msg='ADJ', answer=True)
            if ans == 'SUCCESS':
                self.connected = True
        except:
            print("Failed to Connect")

    def goForward(self):
        try:
            # 1501-1700
            self.sendInstruction.send(msg='GFWD')

        except:
            print("Failed on GFWD")

    def goBack(self):
        try:
            self.sendInstruction.send(msg='GBCK')
        except:
            print("Failed on Ardupix4")

    def stop(self):
        self.sendInstruction.send(msg='DSTOP')

    def brake(self):
        self.sendInstruction.send(msg='BRKE')

    def takeOff(self):
        self.sendInstruction.send(msg='TOFF')

    def safetyOff(self):
        self.sendInstruction.send(msg='SFOF')

    def safetyOn(self):
        self.sendInstruction.send(msg='SFON')

    def readyDrone(self):
        self.sendInstruction.send(msg='RDYD')

    def endConnection(self):
        self.sendInstruction.send(msg='END')

    # Gimbal
    def upInstruction(self):
        self.sendInstruction.send(msg='UP')

    def downInstruction(self):
        self.sendInstruction.send(msg='DOWN')

    def rightInstruction(self):
        self.sendInstruction.send(msg='RIGHT')

    def leftInstruction(self):
        self.sendInstruction.send(msg='LEFT')

    def stopInstruction(self):
        self.sendInstruction.send(msg='GSTOP')

    def frontInstruction(self):
        self.sendInstruction.send(msg='FRONT')

    def endInstruction(self):  # Agregarlo en la logica
        # AL ENVIAR END HAY QUE RECIVIR MENSAJE DE APAGADO, AL LLEGAR TERMINA LA CONEXION
        ans = self.sendInstruction.send(msg='END', answer=True)
        if ans == 'SUCCESS':
            self.connected = False

    def askTelemetry(self):
        # TELE necesitara respuesta
        ans = self.sendInstruction.send(msg='TELE', answer=True)
        if ans != None:
            self.telemetry = ans
            self.ans = None
