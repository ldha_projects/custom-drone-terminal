
import socket
from vidgear.gears import NetGear, VideoGear


class SenderSkt():
    # HOST
    def __init__(self, host="127.0.0.1", port=5455, BUFFER_SIZE=1024):
        self.host = host
        self.port = port
        self.BUFFER_SIZE = BUFFER_SIZE

    # Send
    def send(self, msg="frame", answer=False):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as skt:
            comm = msg
            skt.connect((self.host, self.port))
            skt.send(comm.encode('ascii'))
            if answer:
                data = skt.recv(self.BUFFER_SIZE)
                return data.decode('ascii')


class ReceiverSkt():
    def __init__(self, host="127.0.0.1", port=5455, BUFFER_SIZE=1024):
        self.host = host
        self.port = port
        self.BUFFER_SIZE = BUFFER_SIZE
        self.active = False
        self.skt = None

    def receive(self, listening=5):
        self.skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.skt.bind((self.host, self.port))
        self.skt.listen(listening)

    def recive(self, answer = False):
        if self.active ==False:
            self.receive()
        data = None
        self.conn, self.addr = self.skt.accept()
        with self.conn:
            data = self.conn.recv(self.BUFFER_SIZE).decode('ascii')
            if answer == True:
                pass # Agregar la respuesta para asegurar una comunicación exitosa
        if data == None:
            pass
        elif data == 'CLOSECONN':
            self.conn.close() 

    def close(self):
        self.active = False
        self.conn.close()


class clientVidGear():
    def __init__(self, address='127.0.0.1', port='5454', protocol='tcp', pattern=0, receive_mode=True, logging=True):
        self.options = {'flag': 0, 'copy': False, 'track': False}
        self.client = NetGear(address = address, port=port, protocol=protocol,
                              pattern=pattern, receive_mode=receive_mode, logging=logging, **self.options)

    def receive(self):
        frame = self.client.recv()
        return frame
        #cv2.imshow("winName", frame)
        #if cv2.waitKey(1) & 0xFF == ord('q'):
        #    pass

    def close(self):
        self.client.close()
        # cv2.destroyAllWindows()


class serverVidGear():
    def __init__(self, address='127.0.0.1', port='5454', protocol='tcp',  pattern=0, logging=True):
        self.options = {'flag': 0, 'copy': False, 'track': False}
        self.server = NetGear(address=address, port=port,
                              protocol=protocol,  pattern=pattern, logging=True, **self.options)

    def send(self, frame):
        self.server.send(frame)

    def close(self):
        self.server.close()
