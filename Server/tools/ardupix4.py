import sys
import numpy as np
import socket
import threading
from time import sleep

testIp = '192.168.0.17'


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]

    ###########################################################
    # return s.getsockname()[0]


class ArduPix4():
    def __init__(self, ipaddr):
        self.host_drone = ipaddr  # '172.16.0.4'#socket.gethostname() #IP
        self.port_drone = 9998
        self.buffer_size_drone = 1024
        self.comm_drone = ''

        self.host_gimball = ipaddr  # '172.16.0.4'#socket.gethostname() #IP
        self.port_gimball = 9999
        self.buffer_size_gimball = 1024
        self.comm_gimball = ''
        self.arduStart(ipaddr)
        self.arduMsg(testIp)
        self.connected = None
        self.telemetry = None
        self.skt2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.skt2.connect((self.host_drone, self.port_drone))

    def arduStart(self, ipaddr):
        self.skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.skt.connect((ipaddr, 12345))

    def arduMsg(self, msg):
        # get_ip_address().encode('ascii'))
        self.skt.send(msg.encode('ascii'))
        data = self.skt.recv(1024)
        return(data.decode('ascii'))
        # agregar hacer algo si regresa no permitido o nada

    def arduClose(self):
        self.skt.close()
        self.skt2.close()

    # Drone
    def goForward(self, inclination):
        self.comm_drone = 'GFWD' + inclination
        self.sendDrone(self.comm_drone)

    def goBack(self, inclination):
        self.comm_drone = 'GBCK' + inclination
        self.sendDrone(self.comm_drone)

    def stop(self):
        self.comm_drone = 'DSTOP'
        self.sendDrone(self.comm_drone)

    def brake(self):
        self.comm_drone = 'BRKE'
        self.sendDrone(self.comm_drone)

    def takeOff(self):
        self.comm_drone = 'TOFF'
        self.sendDrone(self.comm_drone)

    def safetyOff(self):
        self.comm_drone = 'SFOF'
        self.sendDrone(self.comm_drone)

    def safetyOn(self):
        self.comm_drone = 'SFON'
        self.sendDrone(self.comm_drone)

    def readyDrone(self):
        self.comm_drone = 'RDYD'
        self.sendDrone(self.comm_drone)

    def endConnection(self):
        self.comm_drone = 'END'
        self.sendDrone(self.comm_drone)

    # Gimbal
    def upInstruction(self):
        self.comm_drone = 'UP'
        self.sendDrone(self.comm_drone)

    def downInstruction(self):
        self.comm_drone = 'DOWN'
        self.sendDrone(self.comm_drone)

    def rightInstruction(self):
        self.comm_drone = 'RIGHT'
        self.sendDrone(self.comm_drone)

    def leftInstruction(self):
        self.comm_drone = 'LEFT'
        self.sendDrone(self.comm_drone)

    def stopInstruction(self):
        self.comm_drone = 'GSTOP'
        self.sendDrone(self.comm_drone)

    def frontInstruction(self):
        self.comm_drone = 'FRONT'
        self.sendDrone(self.comm_drone)

    def endInstruction(self):  # Agregarlo en la logica
        # self.comm_drone = 'END'
        # self.sendDrone(self.comm_drone)
        self.arduMsg('END')
        self.arduClose()

    def askTelemetry(self):
        self.comm_drone = 'TELE'
        self.sendDrone(self.comm_drone)

    def sendDrone(self, comm_drone):
        memoria = comm_drone

        try:
            self.skt2.send(comm_drone.encode('ascii'))
            if memoria == 'TELE':
                data = self.skt2.recv(self.buffer_size_drone)
                self.telemetry = data.decode()
            else:
                data = self.skt2.recv(self.buffer_size_drone)
            self.connected = True
        except:
            print("Failed on Ardupix4")
            self.connected = False

    def sendGimbal(self, comm_gimball):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as skt:
            try:
                skt.connect((self.host_gimball, self.port_gimball))
                skt.send(comm_gimball.encode('ascii'))
                data = skt.recv(self.buffer_size_gimball)
                print(data.decode('ascii'))
            except:
                print("Failed on Ardupix4 Dron")
