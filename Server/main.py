import sys
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget, QInputDialog
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QTimer, QThread, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QMessageBox
import numpy as np
import time
import cv2
from tools import ardupix4
from MainUI.ui_main_window import *
import random
import json
import MissionTools.createMission
from datetime import datetime
import socket
import math
import threading
import time
import pickle
import struct  # new
import zlib
from Login.loginWidget import *
import Settings.settings
from vidgear.gears import NetGear
import os

# CambiobyMirip
# EsteLuis
# Aqui es el que estan usando192.168.0.20


class VideoReceicer(QThread):  # Receives frame
    def __init__(self, ipOrigen='127.0.0.1'):
        super().__init__()
        self.running = False
        self.currentframe = None
        self.options = {'flag': 0, 'copy': False, 'track': False}
        self.client = NetGear(address='192.168.0.17', port='5454', protocol='tcp',
                              pattern=0, receive_mode=True, logging=True, **self.options)

    def run(self):
        while self.running:
            self.currentframe = self.client.recv()

    def close(self):
        self.running = False
        self.client.close()


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


class Main(QtWidgets.QMainWindow):
    # class constructor
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.accessLvl = ""
        self.customDrone = None

        # Station
        self.station = 0
        self.stations = ["0", "1", "2", "3"]
        self.i = 0

        # LogIn
        self.username = ''
        self.password = ''
        self.accessLvl = ""
        self.dialog = loginWidget.LogInWindow()
        self.dialog.set_messages(self.username, self.password)
        self.dialog.submitted.connect(self.update_messages)

        # No video background image
        self.ui.frame.setStyleSheet(
            "QFrame{\n""border-image:url(icons/noVideoimg.png);\n""}")
        self.setLabels()

        # Timer That displays what the tello is facing
        self.timer = QTimer()
        self.timer.timeout.connect(self.viewCam)

        # Timer that excecute auto missions
        self.missionExTimer = QTimer()
        self.missionExTimer.timeout.connect(self.missionExcecuter)

        self.teleTimer = QTimer()
        self.teleTimer.timeout.connect(self.askTelemetry)
        # Shows a FlightTime
        self.flightTimer = QTimer()
        self.flightTimer.timeout.connect(self.timerUp)

        # Stores the frames to record video
        self.recordTimer = QTimer()
        self.recordTimer.timeout.connect(self.recordCam)

        # Cleans the warnings label

        self.cleanWarning = QTimer()
        self.cleanWarning.timeout.connect(self.cleanWarnings)

        # missionExcecuter list
        self.executedSteps = []

        # Clears The warnings on screen
        # self.clearWarnings = QTimer()
        # self.clearWarnings.timeout.connect(self.ui.warnings.setText(""))
        # self.clearWarnings.start(10000)

        # button connection to the function that Start recording
        self.ui.control_bt.clicked.connect(self.controlTimer)

        # button connection to the function that set speeds functions(Connects to the Drone)
        self.ui.power.clicked.connect(self.connectDrone)
        self.ui.power.setEnabled(False)

        # self.ui.rth.clicked.connect(self.)

        # Control Buttons connections
        self.ui.turnLeft.pressed.connect(self.turnLefth)
        self.ui.turnLeft.released.connect(self.stopTurn)
        self.ui.turnRight.pressed.connect(self.turnRight)
        self.ui.turnRight.released.connect(self.stopTurn)
        self.ui.takeUp.clicked.connect(self.startTakeoff)
        self.ui.land.clicked.connect(self.eStop)
        self.ui.goBack.pressed.connect(self.goBack)
        self.ui.goBack.released.connect(self.stop)
        self.ui.goFoward.pressed.connect(self.goFoward)
        self.ui.goFoward.released.connect(self.stop)
        # self.ui.standStill.clicked.connect(self.stop)
        self.ui.goUp.clicked.connect(self.goUp)
        self.ui.goDown.clicked.connect(self.goDown)
        self.ui.eStop.clicked.connect(self.eStop)
        self.ui.missionsAuto.clicked.connect(self.autoMode)
        self.ui.takePhoto.clicked.connect(self.takepicture)
        self.ui.pauseRecording.clicked.connect(self.pauserecording)
        self.ui.rth.clicked.connect(self.returnToHome)

        # self.ui.rth.clicked.connect(self.selectStation)
        self.ui.rth.pressed.connect(self.clickedButton)
        self.ui.rth.released.connect(self.passed3seconds)

        # RTH
        self.rthTimer = QTimer()
        self.rthTimer.timeout.connect(self.timim)

        self.recordCamController = False
        # pauseRecording button
        self.ui.pauseRecording.hide()
        # Log In/out
        self.ui.logout.hide()
        self.ui.logout.clicked.connect(self.logOut)
        # Settings
        self.ui.settings.clicked.connect(settings.EditSettings(self).show)

        # Used when you pause recording
        self.paused = False
        # stores the steps for automatic missions
        self.rutina = []
        self.data = None
        self.minutes = 0
        self.seconds = 0

        # Gets the Missionsthat have been created
        with open("missions.json", "r") as f:
            self.data = json.load(f)
        f.close()
        self.addItemQcombo()

        with open("settings.json", "r") as f:
            self.settingsData = json.load(f)
            f.close()
        self.addDroneList()

    # LogIn
    # Aqui es lo que buscas

    @QtCore.pyqtSlot(str, str, str)  # Update labels when LogIn
    def update_messages(self, username, password, accessLvl):
        print(accessLvl, username, password)
        if accessLvl != "":
            self.ui.logout.show()
            self.accessLvl = accessLvl
            self.ui.power.setEnabled(True)
            if accessLvl == "admin":
                icon3 = QtGui.QIcon()
                icon3.addPixmap(QtGui.QPixmap(".\\icons/drone.png"),
                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                self.ui.settings.setIcon(icon3)
                self.ui.settings.setIconSize(QtCore.QSize(50, 50))
            else:
                icon3 = QtGui.QIcon()
                icon3.addPixmap(QtGui.QPixmap(".\\icons/redDrone.png"),
                                QtGui.QIcon.Normal, QtGui.QIcon.Off)
                self.ui.settings.setIcon(icon3)
                self.ui.settings.setIconSize(QtCore.QSize(50, 50))

        else:
            print("error")

    def openLogIn(self):  # Shows LogIn dialog
        self.dialog.show()

    def logOut(self):  # Logs out from user's current sesion
        reply = QMessageBox.question(
            self, 'Logout', 'Sure', QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.username = ""
            self.password = ""
            self.accessLvl = ""
            icon3 = QtGui.QIcon()
            icon3.addPixmap(QtGui.QPixmap(".\\icons/user.png"),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.ui.settings.setIcon(icon3)
            self.ui.settings.setIconSize(QtCore.QSize(50, 50))
            self.ui.logout.hide()
            self.openLogIn()
            self.ui.power.setEnabled(False)
        else:
            pass

    # __XeemConnection__
    # Function to start the connection to the Tello

    def connectDrone(self):  # Turn On/Off drone
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    self.timer.start()
                    if self.customDrone == None:
                        self.updateSettingsData()
                        self.customDrone = ardupix4.ArduPix4(
                            self.settingsData["drones"][self.ui.droneSelector.currentText()]["IP"])
                    self.customDrone.readyDrone()
                    self.ui.droneSelector.setEnabled(False)
                    self.droneVideo = VideoReceicer()
                    self.droneVideo.running = True
                    self.droneVideo.start()
                    # droneVideo = VideoReciver()
                    # options = {'flag': 0, 'copy': False, 'track': False}
                    # self.client = NetGear(address='192.168.1.218', port='5454', protocol='tcp',
                    #   pattern = 0, receive_mode = True, logging = True, **options)
                    self.ui.warnings.setText("")
                    self.teleTimer.start(5000)
                else:
                    self.turnOff()
                    # release Sockets
        except:
            self.customDrone.stop()
            self.customDrone.stop()
            self.turnOff()

    def turnOff(self):  # Turn Off drone
        try:
            self.teleTimer.stop()
        except:
            print('self.teleTimer.stop()')

        try:
            self.ui.frame.setStyleSheet(
                "QFrame{\n""border-image:url(icons/noVideoimg.png);\n""}")
            self.setLabels()
            try:
                self.customDrone.endConnection()
                self.ui.warnings.setText("Disconect signal sent")
            except Exception as e:
                print(e)
                self.ui.warnings.setText("Failed to disconnect drone")
                print("Failed to disconnect drone")

            try:
                self.timer.stop()
            except:
                self.ui.warnings.setText("except self.timer.stop()")
                print("except self.timer.stop()")
            try:
                self.missionExTimer.stop()
            except:
                self.ui.warnings.setText(" except self.missionExTimer.stop()")
                print(" except self.missionExTimer.stop()")
            try:
                self.executedSteps = []
            except:
                self.ui.warnings.setText("except self.executedSteps = []")
                print("except self.executedSteps = []")
            try:
                self.rutina = []
            except Exception as e:
                print(e)
                self.ui.warnings.setText("except self.rutina = []")
                print("except self.rutina = []")

            try:
                self.ui.power.setChecked(False)
            except:
                self.ui.warnings.setText(
                    "except self.ui.power.setChecked(False)")
                print("except self.ui.power.setChecked(False)")
            try:
                self.ui.warnings.setText("Disconected")
            except:
                self.ui.warnings.setText(
                    "except self.ui.warnings.setText('Disconected')")
                print("except self.ui.warnings.setText('Disconected')")
            try:
                self.ui.droneSelector.setEnabled(True)
            except:
                self.ui.warnings.setText(
                    "except self.ui.droneSelector.setEnabled(True)")
                print("except self.ui.droneSelector.setEnabled(True)")
            try:
                self.ui.batteryLabel.setText('None')
            except:
                self.ui.warnings.setText(
                    "except self.ui.batteryLabel.setText('None')")
                print("except self.ui.batteryLabel.setText('None')")
            try:
                self.ui.latLabel.setText('None')
            except:
                self.ui.warnings.setText(
                    "except self.ui.latLabel.setText('None')")
                print("except self.ui.latLabel.setText('None')")
            try:
                self.ui.lonLabel.setText('None')
            except:
                self.ui.warnings.setText(
                    "except self.ui.lonLabel.setText('None')")
                print("except self.ui.lonLabel.setText('None')")
            try:
                self.customDrone.endInstruction()
            except:
                print("end customDrone")
            try:
                self.customDrone = None
            except:
                self.ui.warnings.setText("except self.customDrone = None")
                print("except self.customDrone = None")
            try:
                self.droneVideo.running = False
            except:
                self.ui.warnings.setText(
                    "except self.droneVideo.running = False")
                print("except self.droneVideo.running = False")
            try:
                self.droneVideo.close()
            except:
                self.ui.warnings.setText("except self.client.close()")
                print("except self.client.close()")

        except:
            # add power off
            self.ui.power.setChecked(False)
            # self.ui.power.setEnabled(False)
            self.ui.warnings.setText("Connection Failed")
            print("QFrame{'border-image: url(icons/noVideoimg.png)'")

    # __addDroneList()__
    def addDroneList(self):  # Updates Qlist with all the drones
        try:
            missions = []
            for x in self.settingsData["drones"]:
                missions.append(x)
            for mission in missions:
                self.ui.droneSelector.addItem(mission)
        except:
            pass

    # __AutoMode__

    def addItemQcombo(self):  # Adds missions to the combobox(dropdown list)
        try:
            missions = []
            for x in self.data:
                missions.append(x)
            for mission in missions:
                self.ui.missionSelector.addItem(mission)
        except:
            pass

    # ----------------------------------------------------
    # starts a Mission
    def autoMode(self):
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    self.rutina = self.data[self.ui.missionSelector.currentText(
                    )]
                    self.missionExTimer.start(500)
        except:
            pass

    def returnToHomee(self):  # Variable RTH (Not completed)
        self.station = 0
        print(self.station)
        print("Home")

    def timim(self):  # Counter RTH (Not completed)
        self.i = self.i + 1

    def clickedButton(self):  # Timer RTH (Not completed)
        self.rthTimer.start(1000)

    def passed3seconds(self):  # Choose station RTH (Not completed)
        if self.i >= 3:
            self.station = 0
            print(self.station)
        else:
            self.rthTimer.start()
            print()
            item, ok = QInputDialog.getItem(
                self, "Edit Mission", "Choose mission:", self.stations, 0, False)
            self.station = int(item)
            print(self.station)
        self.rthTimer.stop()
        self.i = 0

    # Excecutes the steps to follow in a mission
    def missionExcecuter(self):
        try:
            self.executedSteps = []
            if len(self.rutina) <= 0:
                self.ui.flightMode.setText("ended")
                self.missionExTimer.stop()
            else:
                if self.pausaRutina:  # Agregar algo que pueda continuar la rutina
                    self.missionExTimer.stop()
                try:
                    print(self.rutina[0][0])
                    if self.rutina[0][0] == "fwd":
                        self.goFoward()
                    elif self.rutina[0][0] == "bck":
                        self.goBack()
                    elif self.rutina[0][0] == "trt":
                        self.turnRight()
                    elif self.rutina[0][0] == "tlt":
                        self.turnLefth()
                    elif self.rutina[0][0] == "stp":
                        self.stop()
                    elif self.rutina[0][0] == "end":  # end recording
                        if 'str' in self.executedSteps:
                            if 'psa' in self.executedSteps:
                                self.executedSteps.remove('psa')
                                self.pauserecording()
                            self.executedSteps.remove('str')
                            print("endRecording")
                            self.controlTimer()
                        else:
                            pass
                    elif self.rutina[0][0] == "str":
                        if 'str' in self.executedSteps:
                            pass
                        else:
                            print("Recording")
                            self.controlTimer()
                            self.executedSteps.append('str')
                    elif self.rutina[0][0] == "psa":
                        if 'str' in self.executedSteps:
                            if 'psa' in self.executedSteps:
                                self.executedSteps.remove('psa')
                            else:
                                self.executedSteps.append('psa')
                            self.pauserecording()
                        else:
                            pass
                    elif self.rutina[0][0] == "pht":
                        self.takepicture()
                        pass
                    elif self.rutina[0][0] == "tof":
                        if 'tof' in self.executedSteps:
                            pass
                        else:
                            self.startTakeoff()
                            self.executedSteps.append('tof')
                    elif self.rutina[0][0] == "lnd":
                        self.startLanding()
                except:
                    pass
                self.missionExTimer.start(self.rutina[0][1]*1000)
                del self.rutina[:1]
        except:
            pass

    def pausarRutina(self):  # Pauses current routine
        try:
            self.pausaRutina = not self.pausaRutina
        except:
            pass

    def stopRoutine(self):  # Stops current routine
        try:
            del self.rutina[:len(self.rutina)]
            self.stop()
        except:
            pass

    # __Camera__
    # view camera
    def viewCam(self):
        try:
            # frame = self.client.recv()
            frame = self.droneVideo.currentframe
            if frame is None:
                pass
            else:
                if self.ui.batteryLabel.text() != 'NULL':
                    # {do something with the received frame here}d
                    self.updateSettingsData()
                    cv2.imwrite(
                        self.settingsData["droneParameters"]["saveimgDunas"] + '/tmpimg.jpg', frame)
                    self.ui.frame.setStyleSheet(
                        "QFrame{\n""border-image:url(" + self.settingsData['droneParameters']['saveimgDunas']+"/tmpImg.jpg);\n""}")
                    self.setLabels()
                else:
                    self.ui.warnings.setText(
                        'Connecting to Drone, Please Wait...')
            if self.recordCamController:
                try:
                    self.recordCam(frame)
                except:
                    pass
            os.remove('tmpimg.jpg')
        except:
            pass

    # start/stop Recording
    def controlTimer(self):
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    # if timer is stopped
                    if self.recordCamController == False:
                        self.ui.pauseRecording.show()
                        self.out = cv2.VideoWriter(self.settingsData["droneParameters"]["saveimgDunas"] + "/" + datetime.now(
                        ).strftime("%d_%m_%Y, %H-%M-%S")+'.avi', cv2.VideoWriter_fourcc("M", "J", "P", "G"), 27.0, (960, 720))
                        self.recordCamController = True
                        # update control_bt icon
                        icon7 = QtGui.QIcon()
                        icon7.addPixmap(QtGui.QPixmap(
                            ".\\icons/recording.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                        self.ui.control_bt.setIcon(icon7)
                        self.ui.control_bt.setIconSize(QtCore.QSize(51, 51))
                    elif self.recordCamController == True:
                        # update control_bt icon
                        self.out.release()  # Release the variable to create a newone and store another video
                        self.ui.pauseRecording.hide()
                        self.recordCamController = False
                        icon7 = QtGui.QIcon()
                        icon7.addPixmap(QtGui.QPixmap(
                            ".\\icons/rec.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                        self.ui.control_bt.setIcon(icon7)
                        self.ui.control_bt.setIconSize(QtCore.QSize(51, 51))
        except:
            pass

    def recordCam(self, recimg):  # Writes the frame
        try:
            if not self.paused:
                # recimg = cv2.imread('tmpimg.jpg')
                resized = cv2.resize(recimg, (960, 720),
                                     interpolation=cv2.INTER_AREA)
                self.out.write(resized)
            elif self.paused:
                pass
        except:
            pass

    def pauserecording(self):  # Changes "self.paused" variable's status
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if not self.paused:
                    self.paused = True
                elif self.paused:
                    self.paused = False
        except:
            pass

    def takepicture(self):  # Takes picture
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    self.updateSettingsData()
                    cv2.imwrite(self.settingsData["droneParameters"]["saveimgDunas"] + "/" + datetime.now().strftime(
                        "%d_%m_%Y, %H-%M-%S")+'.bmp', cv2.imread(self.settingsData["droneParameters"]["saveimgDunas"] + '/tmpimg.jpg'))
        except:
            pass

    # __flightTime__
    def timerUp(self):
        try:
            self.seconds += 1
            if self.seconds == 60:
                self.seconds = 0
                self.minutes += 1
            self.ui.flightTime.setText(
                str(self.minutes) + ":" + str(self.seconds))
        except:
            pass

    # __Telemetry__
    def updateStatus(self):
        try:
            if self.ui.power.isChecked():
                self.ui.batteryLabel.setText(self.battery)
                self.ui.latLabel.setText(str(int(self.tof)))
                self.ui.lonLabel.setText(str(self.yaw))
                # add self.ui.Nombre De La Label.setText(self.yaw)
        except:
            self.turnOff()

    # __Movement__
    # Movement fucnctions
    def goFoward(self):  # Sends 'GFWD' instruction (Go fordward)
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    try:  # if self.customDrone.connected in locals() or self.customDrone.connected in globals():
                        if self.customDrone.connected == True:
                            inclination = str(
                                1500 + ((self.ui.velocity_VerticalSlider.value() + 1) * 2))
                            self.customDrone.goForward(inclination)
                        else:
                            self.turnOff()
                    except:
                        pass
                else:
                    self.turnOff()
        except:
            self.turnOff()

    def goBack(self):  # Sends 'GBCK' instruction (Go backwards)
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    try:  # if self.customDrone.connected in locals() or self.customDrone.connected in globals():
                        if self.customDrone.connected == True:
                            inclination = str(
                                1500 - ((self.ui.velocity_VerticalSlider.value() + 1) * 2))
                            self.customDrone.goBack(inclination)
                        else:
                            self.turnOff()
                    except:
                        pass
                else:
                    self.turnOff()
        except:
            self.turnOff()

    def goUp(self):  # Send 'FRONT' instruction
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    try:  # if self.customDrone.connected in locals() or self.customDrone.connected in globals():
                        if self.customDrone.connected == True:
                            self.customDrone.frontInstruction()
                        else:
                            self.turnOff()
                    except:
                        pass
                else:
                    self.turnOff()
        except:
            self.turnOff()

    def goDown(self):  # Sends 'DOWN' instruction (Go down)
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    try:  # if self.customDrone.connected in locals() or self.customDrone.connected in globals():
                        if self.customDrone.connected == True:
                            self.customDrone.downInstruction()
                        else:
                            self.turnOff()
                    except:
                        pass
                else:
                    self.turnOff()
        except:
            self.turnOff()

    def terminateGimbal(self):  # Sends 'END' command
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    self.customDrone.endConnection()
        except:
            pass

    def turnRight(self):  # Sends 'RIGHT' instruction (Go right)
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    try:  # if self.customDrone.connected in locals() or self.customDrone.connected in globals():
                        if self.customDrone.connected == True:
                            self.customDrone.rightInstruction()
                        else:
                            self.turnOff()
                    except:
                        pass
                else:
                    self.turnOff()
        except:
            self.turnOff()

    def turnLefth(self):  # Sends 'LEFT' instruction
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    try:  # if self.customDrone.connected in locals() or self.customDrone.connected in globals():
                        if self.customDrone.connected == True:
                            self.customDrone.leftInstruction()
                        else:
                            self.turnOff()
                    except:
                        pass
                else:
                    self.turnOff()
        except:
            self.turnOff()

    def stopTurn(self):  # Sends 'GSTOP' instruction
        if self.accessLvl == "":
            self.openLogIn()
        else:
            if self.ui.power.isChecked():
                self.customDrone.stopInstruction()
            else:
                pass

    def stop(self):  # Sends 'BRKE' instruction
        if self.accessLvl == "":
            self.openLogIn()
        else:
            if self.ui.power.isChecked():
                self.customDrone.brake()
            else:
                pass

    # State Functions

    def startTakeoff(self):  # Sends 'TOFF' instruction
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    try:  # if self.customDrone.connected in locals() or self.customDrone.connected in globals():
                        if self.customDrone.connected == True:
                            self.customDrone.takeOff()
                        else:
                            self.turnOff()
                    except:
                        pass
                else:
                    self.turnOff()
        except:
            self.turnOff()

    def startLanding(self):  # Sends 'DSTOP' instruction
        self.customDrone.stop()

    def eStop(self):  # Emergency Stop (Sends 'DSTOP' instruction)
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                try:  # if self.customDrone.connected in locals() or self.customDrone.connected in globals():
                    if self.customDrone.connected == True:
                        self.customDrone.stop()
                    else:
                        self.turnOff()
                except:
                    pass
        except:
            self.turnOff()

    def returnToHome(self):  # ?
        if self.accessLvl == "":
            self.openLogIn()
        else:
            self.goBack()

    def updateSettingsData(self):  # Gets json's data
        with open("settings.json", "r") as f:
            self.settingsData = json.load(f)
            f.close()

    def askTelemetry(self):  # Asks for telemetry/Set labels telemetry values
        self.customDrone.askTelemetry()

        try:
            tele = self.customDrone.telemetry.split(":")
            self.ui.batteryLabel.setText(str(int(float(tele[2])))+"%")
            self.ui.latLabel.setText(str(float(tele[1])))
            self.ui.lonLabel.setText(str(float(tele[0])))
            self.teleTimer.start(5000)
        except:
            pass
        # print(tele)

    def getTele(self):  # Asks for telemetry
        try:
            if self.accessLvl == "":
                self.openLogIn()
            else:
                if self.ui.power.isChecked():
                    self.askTelemetry()
                else:
                    self.turnOff()
        except:
            self.turnOff()

    def setLabels(self):  # Set labels to background-color: transparent
        self.ui.warnings.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")
        self.ui.flightMode.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")
        self.ui.flightTime.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")
        self.ui.label_2.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")
        self.ui.label_5.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")
        self.ui.label_7.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")
        self.ui.latLabel.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")
        self.ui.lonLabel.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")
        self.ui.batteryLabel.setStyleSheet(
            "border-image:none;\nfont-size: 20px;\nfont-weight: bold;\nbackground-color: transparent;\ncolor: rgb(255,255,255);\n")

    def setWarnings(self, text):
        self.ui.warnings.setText(text)
        self.cleanWarning.start(3000)

    def cleanWarnings(self):
        self.ui.warnings.setText("")
        self.cleanWarning.stop()

    def closeEvent(self, event):  # Close main window
        try:
            reply = QMessageBox.question(
                self, 'Close application', 'Are you sure you want to close the window?', QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.Yes:
                # Verificar que el dron esta en home y apagar todo
                event.accept()
            else:
                event.ignore()
        except:
            pass


def main_window():  # Run application
    app = QApplication(sys.argv)

    # create and show mainWindow
    mainWindow = Main()
    mainWindow.showMaximized()
    mainWindow.openLogIn()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main_window()
