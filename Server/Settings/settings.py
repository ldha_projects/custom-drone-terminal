import sys
import json
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget,  QInputDialog, QDialog, QLineEdit, QMessageBox, QFileDialog
from PyQt5.QtGui import QImage
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QTimer
import numpy as np
import time
from Settings.ui_settings import *
import MissionTools.createMission
import DroneTools.droneCreator
from Users.userCreator import UserCreator


class EditSettings(QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.parent = parent
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.changes = False

        # Buttons
        self.ui.missionCreator.clicked.connect(
            createMission.MissionCreatorDrone().show)
        self.ui.saveimgDunas.clicked.connect(self.choosePathImg)
        self.ui.registerDroneButton.clicked.connect(
            droneCreator.CreateDrone().show)  # falta
        self.ui.save.clicked.connect(self.close)
        self.ui.createUserButton.clicked.connect(UserCreator().show)

        # open settings Json
        self.openJson()

        # General tab

        # Drone tab
        self.ui.adjustVelocity.setCurrentIndex(
            self.data["droneParameters"]["adjustVelocity"][0])
        self.ui.imgFormatDunas.setCurrentIndex(
            self.data["droneParameters"]["imgFormatDunas"][0])
        self.ui.videoResDunas.setCurrentIndex(
            self.data["droneParameters"]["videoResDunas"][0])
        self.ui.saveimgDunas.setText(
            self.data["droneParameters"]["saveimgDunas"])

    def choosePathImg(self):
        self.dir_path = QFileDialog.getExistingDirectory(
            self, "Choose Directory", "C:\\")
        self.ui.saveimgDunas.setText(self.dir_path)

    def checkChanges(self):
        self.openJson()
        if self.data["droneParameters"] != {"adjustVelocity": [self.ui.adjustVelocity.currentIndex(), self.ui.adjustVelocity.currentText()], "imgFormatDunas": [self.ui.imgFormatDunas.currentIndex(), self.ui.imgFormatDunas.currentText()], "videoResDunas": [self.ui.videoResDunas.currentIndex(), self.ui.videoResDunas.currentText()], "saveimgDunas": self.ui.saveimgDunas.text()}:
            self.changes = True
        else:
            self.changes = False

    def openJson(self):
        try:
            with open("settings.json", "r") as settingsData:
                self.data = json.load(settingsData)
                settingsData.close()
        except:
            settingsData = open("settings.json", "w+")
            self.data = {"droneParameters": {"adjustVelocity": [0, "Fast"], "imgFormatDunas": [
                0, "JPEG"], "videoResDunas": [0, "HD"], "saveimgDunas": "C:\\"}, "drones": {}}
            json.dump(self.data, settingsData, indent=4)
            settingsData.close()

    def closeEvent(self, event):
        try:
            self.checkChanges()
            if self.changes == True:
                question = QMessageBox.question(
                    self, 'Save changes', 'You have made some changes, do you want to save them all?', QMessageBox.Yes | QMessageBox.No)
                if question == QMessageBox.Yes:
                    self.openJson()
                    self.data["droneParameters"] = {"adjustVelocity": [self.ui.adjustVelocity.currentIndex(), self.ui.adjustVelocity.currentText()], "imgFormatDunas": [self.ui.imgFormatDunas.currentIndex(
                    ), self.ui.imgFormatDunas.currentText()], "videoResDunas": [self.ui.videoResDunas.currentIndex(), self.ui.videoResDunas.currentText()], "saveimgDunas": self.ui.saveimgDunas.text()}
                    with open('settings.json', 'w') as file:
                        json.dump(self.data, file, indent=4)
                    event.accept()
                else:
                    event.accept()
            else:
                pass
        except:
            pass


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QDialog()
    ui = EditSettings()
    ui.show()
    sys.exit(app.exec_())
