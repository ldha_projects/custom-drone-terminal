# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\ui_settings.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(600, 703)
        Dialog.setMaximumSize(QtCore.QSize(600, 16777215))
        Dialog.setStyleSheet("background-color: rgb(53, 53, 53);\n"
"border:none;")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.groupBox_4 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_4.setTitle("")
        self.groupBox_4.setObjectName("groupBox_4")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox_4)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_8 = QtWidgets.QPushButton(self.groupBox_4)
        self.pushButton_8.setMinimumSize(QtCore.QSize(30, 0))
        self.pushButton_8.setMaximumSize(QtCore.QSize(30, 16777215))
        self.pushButton_8.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(".\\icons/settings.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_8.setIcon(icon)
        self.pushButton_8.setObjectName("pushButton_8")
        self.horizontalLayout.addWidget(self.pushButton_8)
        self.label_5 = QtWidgets.QLabel(self.groupBox_4)
        self.label_5.setStyleSheet("color:rgb(255, 255, 255);\n"
"font: 75 18pt \"Microsoft YaHei\";")
        self.label_5.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout.addWidget(self.label_5)
        self.verticalLayout_3.addWidget(self.groupBox_4)
        self.groupBox_2 = QtWidgets.QGroupBox(Dialog)
        self.groupBox_2.setStyleSheet("border:none")
        self.groupBox_2.setTitle("")
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout_67 = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_67.setContentsMargins(0, 0, 0, -1)
        self.horizontalLayout_67.setObjectName("horizontalLayout_67")
        self.tabWidget = QtWidgets.QTabWidget(self.groupBox_2)
        self.tabWidget.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setKerning(False)
        self.tabWidget.setFont(font)
        self.tabWidget.setMouseTracking(False)
        self.tabWidget.setStyleSheet("QTabWidget:pane {\n"
"    border: 0px; padding: 0px; margin:0px\n"
"}\n"
"QTabBar::tab\n"
"{\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: transparent;\n"
"    padding: 12px;\n"
"    border:none;\n"
"    border-radius:15px;\n"
"}\n"
"\n"
"QTabBar::tab:selected, \n"
"QTabBar::tab:hover \n"
"{\n"
"    border-top:3px solid rgb(39, 39, 39);\n"
"    background-color: rgb(45, 45, 45);\n"
"}")
        self.tabWidget.setTabPosition(QtWidgets.QTabWidget.North)
        self.tabWidget.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.tabWidget.setUsesScrollButtons(True)
        self.tabWidget.setTabsClosable(False)
        self.tabWidget.setMovable(False)
        self.tabWidget.setTabBarAutoHide(False)
        self.tabWidget.setObjectName("tabWidget")
        self.general = QtWidgets.QWidget()
        self.general.setStyleSheet("border:none")
        self.general.setObjectName("general")
        self.horizontalLayout_40 = QtWidgets.QHBoxLayout(self.general)
        self.horizontalLayout_40.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_40.setObjectName("horizontalLayout_40")
        self.groupBox_31 = QtWidgets.QGroupBox(self.general)
        self.groupBox_31.setStyleSheet("border:none")
        self.groupBox_31.setTitle("")
        self.groupBox_31.setObjectName("groupBox_31")
        self.verticalLayout_15 = QtWidgets.QVBoxLayout(self.groupBox_31)
        self.verticalLayout_15.setContentsMargins(-1, 0, -1, -1)
        self.verticalLayout_15.setObjectName("verticalLayout_15")
        self.groupBox_32 = QtWidgets.QGroupBox(self.groupBox_31)
        self.groupBox_32.setStyleSheet("border:none")
        self.groupBox_32.setTitle("")
        self.groupBox_32.setObjectName("groupBox_32")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.groupBox_32)
        self.verticalLayout_5.setContentsMargins(-1, 0, -1, -1)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.horizontalLayout_93 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_93.setSpacing(0)
        self.horizontalLayout_93.setObjectName("horizontalLayout_93")
        self.label_62 = QtWidgets.QLabel(self.groupBox_32)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_62.sizePolicy().hasHeightForWidth())
        self.label_62.setSizePolicy(sizePolicy)
        self.label_62.setMinimumSize(QtCore.QSize(170, 0))
        self.label_62.setMaximumSize(QtCore.QSize(170, 16777215))
        self.label_62.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_62.setObjectName("label_62")
        self.horizontalLayout_93.addWidget(self.label_62)
        spacerItem = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_93.addItem(spacerItem)
        self.registerDroneButton = QtWidgets.QPushButton(self.groupBox_32)
        self.registerDroneButton.setMinimumSize(QtCore.QSize(0, 45))
        self.registerDroneButton.setMaximumSize(QtCore.QSize(200, 16777215))
        self.registerDroneButton.setSizeIncrement(QtCore.QSize(300, 0))
        self.registerDroneButton.setStyleSheet("QPushButton{\n"
"    border-radius:15px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: qlineargradient(spread:pad, x1:0.502, y1:1, x2:0.503, y2:0.034, stop:0 rgba(45, 45, 45, 255), stop:0.18408 rgba(60, 60, 60, 255), stop:0.482587 rgba(81, 81, 81, 255), stop:0.751244 rgba(60, 60, 60, 255), stop:1 rgba(45, 45, 45, 255));\n"
"    \n"
"}\n"
"QPushButton:hover{\n"
"    background-color: qlineargradient(spread:pad, x1:0.502, y1:1, x2:0.503, y2:0.034, stop:0 rgba(50, 50, 50, 255), stop:0.18408 rgba(70, 70, 70, 255), stop:0.482587 rgba(90, 90, 90, 255), stop:0.751244 rgba(70, 70, 70, 255), stop:1 rgba(50, 50, 50, 255));\n"
"}")
        self.registerDroneButton.setObjectName("registerDroneButton")
        self.horizontalLayout_93.addWidget(self.registerDroneButton)
        self.verticalLayout_5.addLayout(self.horizontalLayout_93)
        self.horizontalLayout_91 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_91.setSpacing(0)
        self.horizontalLayout_91.setObjectName("horizontalLayout_91")
        self.label_60 = QtWidgets.QLabel(self.groupBox_32)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_60.sizePolicy().hasHeightForWidth())
        self.label_60.setSizePolicy(sizePolicy)
        self.label_60.setMinimumSize(QtCore.QSize(170, 0))
        self.label_60.setMaximumSize(QtCore.QSize(170, 16777215))
        self.label_60.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_60.setObjectName("label_60")
        self.horizontalLayout_91.addWidget(self.label_60)
        spacerItem1 = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_91.addItem(spacerItem1)
        self.missionCreator = QtWidgets.QPushButton(self.groupBox_32)
        self.missionCreator.setMinimumSize(QtCore.QSize(0, 45))
        self.missionCreator.setMaximumSize(QtCore.QSize(200, 16777215))
        self.missionCreator.setSizeIncrement(QtCore.QSize(300, 0))
        self.missionCreator.setStyleSheet("QPushButton{\n"
"    border-radius:15px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: qlineargradient(spread:pad, x1:0.502, y1:1, x2:0.503, y2:0.034, stop:0 rgba(45, 45, 45, 255), stop:0.18408 rgba(60, 60, 60, 255), stop:0.482587 rgba(81, 81, 81, 255), stop:0.751244 rgba(60, 60, 60, 255), stop:1 rgba(45, 45, 45, 255));\n"
"    \n"
"}\n"
"QPushButton:hover{\n"
"    background-color: qlineargradient(spread:pad, x1:0.502, y1:1, x2:0.503, y2:0.034, stop:0 rgba(50, 50, 50, 255), stop:0.18408 rgba(70, 70, 70, 255), stop:0.482587 rgba(90, 90, 90, 255), stop:0.751244 rgba(70, 70, 70, 255), stop:1 rgba(50, 50, 50, 255));\n"
"}")
        self.missionCreator.setObjectName("missionCreator")
        self.horizontalLayout_91.addWidget(self.missionCreator)
        self.verticalLayout_5.addLayout(self.horizontalLayout_91)
        self.horizontalLayout_98 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_98.setSpacing(0)
        self.horizontalLayout_98.setObjectName("horizontalLayout_98")
        self.label_67 = QtWidgets.QLabel(self.groupBox_32)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_67.sizePolicy().hasHeightForWidth())
        self.label_67.setSizePolicy(sizePolicy)
        self.label_67.setMinimumSize(QtCore.QSize(170, 0))
        self.label_67.setMaximumSize(QtCore.QSize(170, 16777215))
        self.label_67.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_67.setObjectName("label_67")
        self.horizontalLayout_98.addWidget(self.label_67)
        spacerItem2 = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_98.addItem(spacerItem2)
        self.chargeButton = QtWidgets.QPushButton(self.groupBox_32)
        self.chargeButton.setMinimumSize(QtCore.QSize(0, 45))
        self.chargeButton.setMaximumSize(QtCore.QSize(200, 16777215))
        self.chargeButton.setSizeIncrement(QtCore.QSize(300, 0))
        self.chargeButton.setStyleSheet("QPushButton{\n"
"    border-radius:15px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: qlineargradient(spread:pad, x1:0.502, y1:1, x2:0.503, y2:0.034, stop:0 rgba(45, 45, 45, 255), stop:0.18408 rgba(60, 60, 60, 255), stop:0.482587 rgba(81, 81, 81, 255), stop:0.751244 rgba(60, 60, 60, 255), stop:1 rgba(45, 45, 45, 255));\n"
"    \n"
"}\n"
"QPushButton:hover{\n"
"    background-color: qlineargradient(spread:pad, x1:0.502, y1:1, x2:0.503, y2:0.034, stop:0 rgba(50, 50, 50, 255), stop:0.18408 rgba(70, 70, 70, 255), stop:0.482587 rgba(90, 90, 90, 255), stop:0.751244 rgba(70, 70, 70, 255), stop:1 rgba(50, 50, 50, 255));\n"
"}")
        self.chargeButton.setObjectName("chargeButton")
        self.horizontalLayout_98.addWidget(self.chargeButton)
        self.verticalLayout_5.addLayout(self.horizontalLayout_98)
        self.horizontalLayout_100 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_100.setSpacing(0)
        self.horizontalLayout_100.setObjectName("horizontalLayout_100")
        self.label_69 = QtWidgets.QLabel(self.groupBox_32)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_69.sizePolicy().hasHeightForWidth())
        self.label_69.setSizePolicy(sizePolicy)
        self.label_69.setMinimumSize(QtCore.QSize(170, 0))
        self.label_69.setMaximumSize(QtCore.QSize(170, 16777215))
        self.label_69.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_69.setObjectName("label_69")
        self.horizontalLayout_100.addWidget(self.label_69)
        spacerItem3 = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_100.addItem(spacerItem3)
        self.createUserButton = QtWidgets.QPushButton(self.groupBox_32)
        self.createUserButton.setMinimumSize(QtCore.QSize(0, 45))
        self.createUserButton.setMaximumSize(QtCore.QSize(200, 16777215))
        self.createUserButton.setSizeIncrement(QtCore.QSize(300, 0))
        self.createUserButton.setStyleSheet("QPushButton{\n"
"    border-radius:15px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: qlineargradient(spread:pad, x1:0.502, y1:1, x2:0.503, y2:0.034, stop:0 rgba(45, 45, 45, 255), stop:0.18408 rgba(60, 60, 60, 255), stop:0.482587 rgba(81, 81, 81, 255), stop:0.751244 rgba(60, 60, 60, 255), stop:1 rgba(45, 45, 45, 255));\n"
"    \n"
"}\n"
"QPushButton:hover{\n"
"    background-color: qlineargradient(spread:pad, x1:0.502, y1:1, x2:0.503, y2:0.034, stop:0 rgba(50, 50, 50, 255), stop:0.18408 rgba(70, 70, 70, 255), stop:0.482587 rgba(90, 90, 90, 255), stop:0.751244 rgba(70, 70, 70, 255), stop:1 rgba(50, 50, 50, 255));\n"
"}")
        self.createUserButton.setObjectName("createUserButton")
        self.horizontalLayout_100.addWidget(self.createUserButton)
        self.verticalLayout_5.addLayout(self.horizontalLayout_100)
        self.verticalLayout_15.addWidget(self.groupBox_32)
        self.horizontalLayout_40.addWidget(self.groupBox_31)
        self.tabWidget.addTab(self.general, "")
        self.Drone = QtWidgets.QWidget()
        self.Drone.setObjectName("Drone")
        self.verticalLayout_12 = QtWidgets.QVBoxLayout(self.Drone)
        self.verticalLayout_12.setObjectName("verticalLayout_12")
        self.groupBox_27 = QtWidgets.QGroupBox(self.Drone)
        self.groupBox_27.setTitle("")
        self.groupBox_27.setObjectName("groupBox_27")
        self.verticalLayout_13 = QtWidgets.QVBoxLayout(self.groupBox_27)
        self.verticalLayout_13.setObjectName("verticalLayout_13")
        self.groupBox_29 = QtWidgets.QGroupBox(self.groupBox_27)
        self.groupBox_29.setTitle("")
        self.groupBox_29.setObjectName("groupBox_29")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox_29)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_97 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_97.setSpacing(0)
        self.horizontalLayout_97.setObjectName("horizontalLayout_97")
        self.label_65 = QtWidgets.QLabel(self.groupBox_29)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_65.sizePolicy().hasHeightForWidth())
        self.label_65.setSizePolicy(sizePolicy)
        self.label_65.setMinimumSize(QtCore.QSize(0, 0))
        self.label_65.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.label_65.setStyleSheet("color:  rgb(117, 117, 117);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_65.setObjectName("label_65")
        self.horizontalLayout_97.addWidget(self.label_65)
        self.line_5 = QtWidgets.QFrame(self.groupBox_29)
        self.line_5.setMinimumSize(QtCore.QSize(0, 1))
        self.line_5.setSizeIncrement(QtCore.QSize(0, 1))
        self.line_5.setStyleSheet("background-color: rgb(50, 50, 50);\n"
"")
        self.line_5.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.horizontalLayout_97.addWidget(self.line_5)
        self.verticalLayout_2.addLayout(self.horizontalLayout_97)
        self.horizontalLayout_92 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_92.setSpacing(0)
        self.horizontalLayout_92.setObjectName("horizontalLayout_92")
        self.label_61 = QtWidgets.QLabel(self.groupBox_29)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_61.sizePolicy().hasHeightForWidth())
        self.label_61.setSizePolicy(sizePolicy)
        self.label_61.setMinimumSize(QtCore.QSize(170, 0))
        self.label_61.setMaximumSize(QtCore.QSize(170, 16777215))
        self.label_61.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_61.setObjectName("label_61")
        self.horizontalLayout_92.addWidget(self.label_61)
        spacerItem4 = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_92.addItem(spacerItem4)
        self.adjustVelocity = QtWidgets.QComboBox(self.groupBox_29)
        self.adjustVelocity.setMinimumSize(QtCore.QSize(0, 30))
        self.adjustVelocity.setMaximumSize(QtCore.QSize(200, 16777215))
        self.adjustVelocity.setStyleSheet("QComboBox{\n"
"    border-radius:7px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(45, 45, 45);\n"
"    padding-left:75px;\n"
"}\n"
"\n"
"\n"
"QListView{\n"
"    border: none;\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(50, 50, 50);\n"
"    font-weight: bold;\n"
"    selection-background-color: rgb(45, 45, 45);\n"
"}\n"
"")
        self.adjustVelocity.setObjectName("adjustVelocity")
        self.adjustVelocity.addItem("")
        self.adjustVelocity.addItem("")
        self.horizontalLayout_92.addWidget(self.adjustVelocity)
        self.verticalLayout_2.addLayout(self.horizontalLayout_92)
        self.horizontalLayout_95 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_95.setSpacing(0)
        self.horizontalLayout_95.setObjectName("horizontalLayout_95")
        self.label_63 = QtWidgets.QLabel(self.groupBox_29)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_63.sizePolicy().hasHeightForWidth())
        self.label_63.setSizePolicy(sizePolicy)
        self.label_63.setMinimumSize(QtCore.QSize(0, 0))
        self.label_63.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.label_63.setStyleSheet("color: rgb(117, 117, 117);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_63.setObjectName("label_63")
        self.horizontalLayout_95.addWidget(self.label_63)
        self.line = QtWidgets.QFrame(self.groupBox_29)
        self.line.setMinimumSize(QtCore.QSize(0, 1))
        self.line.setSizeIncrement(QtCore.QSize(0, 1))
        self.line.setStyleSheet("background-color: rgb(50, 50, 50);\n"
"")
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout_95.addWidget(self.line)
        self.verticalLayout_2.addLayout(self.horizontalLayout_95)
        self.horizontalLayout_72 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_72.setSpacing(0)
        self.horizontalLayout_72.setObjectName("horizontalLayout_72")
        self.label_41 = QtWidgets.QLabel(self.groupBox_29)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_41.sizePolicy().hasHeightForWidth())
        self.label_41.setSizePolicy(sizePolicy)
        self.label_41.setMinimumSize(QtCore.QSize(170, 0))
        self.label_41.setMaximumSize(QtCore.QSize(170, 16777215))
        self.label_41.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_41.setObjectName("label_41")
        self.horizontalLayout_72.addWidget(self.label_41)
        spacerItem5 = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_72.addItem(spacerItem5)
        self.imgFormatDunas = QtWidgets.QComboBox(self.groupBox_29)
        self.imgFormatDunas.setMinimumSize(QtCore.QSize(0, 30))
        self.imgFormatDunas.setMaximumSize(QtCore.QSize(200, 16777215))
        self.imgFormatDunas.setStyleSheet("QComboBox{\n"
"    border:none;\n"
"    border-radius:7px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(45, 45, 45);\n"
"    padding-left:75px\n"
"}\n"
"\n"
"QListView{\n"
"    border: none;\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(50, 50, 50);\n"
"    font-weight: bold;\n"
"    selection-background-color: rgb(45, 45, 45);\n"
"}\n"
"\n"
"QComboBox::down-arrow\n"
"{   \n"
"    border-radius:0px;\n"
"}\n"
"")
        self.imgFormatDunas.setFrame(True)
        self.imgFormatDunas.setObjectName("imgFormatDunas")
        self.imgFormatDunas.addItem("")
        self.imgFormatDunas.addItem("")
        self.horizontalLayout_72.addWidget(self.imgFormatDunas)
        self.verticalLayout_2.addLayout(self.horizontalLayout_72)
        self.horizontalLayout_74 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_74.setSpacing(0)
        self.horizontalLayout_74.setObjectName("horizontalLayout_74")
        self.label_43 = QtWidgets.QLabel(self.groupBox_29)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_43.sizePolicy().hasHeightForWidth())
        self.label_43.setSizePolicy(sizePolicy)
        self.label_43.setMinimumSize(QtCore.QSize(170, 0))
        self.label_43.setMaximumSize(QtCore.QSize(170, 16777215))
        self.label_43.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_43.setObjectName("label_43")
        self.horizontalLayout_74.addWidget(self.label_43)
        spacerItem6 = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_74.addItem(spacerItem6)
        self.videoResDunas = QtWidgets.QComboBox(self.groupBox_29)
        self.videoResDunas.setMinimumSize(QtCore.QSize(0, 30))
        self.videoResDunas.setMaximumSize(QtCore.QSize(200, 16777215))
        self.videoResDunas.setStyleSheet("QComboBox{\n"
"    border-radius:7px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(45, 45, 45);\n"
"padding-left:75px;\n"
"}\n"
"\n"
"\n"
"QListView{\n"
"    border: none;\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(50, 50, 50);\n"
"    font-weight: bold;\n"
"    selection-background-color: rgb(45, 45, 45);\n"
"}\n"
"")
        self.videoResDunas.setObjectName("videoResDunas")
        self.videoResDunas.addItem("")
        self.videoResDunas.addItem("")
        self.videoResDunas.addItem("")
        self.videoResDunas.addItem("")
        self.horizontalLayout_74.addWidget(self.videoResDunas)
        self.verticalLayout_2.addLayout(self.horizontalLayout_74)
        self.horizontalLayout_76 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_76.setSpacing(0)
        self.horizontalLayout_76.setObjectName("horizontalLayout_76")
        self.label_45 = QtWidgets.QLabel(self.groupBox_29)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_45.sizePolicy().hasHeightForWidth())
        self.label_45.setSizePolicy(sizePolicy)
        self.label_45.setMinimumSize(QtCore.QSize(170, 0))
        self.label_45.setMaximumSize(QtCore.QSize(170, 16777215))
        self.label_45.setStyleSheet("color: rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.label_45.setObjectName("label_45")
        self.horizontalLayout_76.addWidget(self.label_45)
        spacerItem7 = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_76.addItem(spacerItem7)
        self.saveimgDunas = QtWidgets.QPushButton(self.groupBox_29)
        self.saveimgDunas.setMinimumSize(QtCore.QSize(0, 30))
        self.saveimgDunas.setMaximumSize(QtCore.QSize(200, 16777215))
        self.saveimgDunas.setStyleSheet("QPushButton{\n"
"    border-radius:7px;\n"
"    font: 12pt \"MS Shell Dlg 2\";\n"
"    color: rgb(255, 255, 255);\n"
"    background-color: rgb(45, 45, 45);\n"
"}\n"
"QPushButton:hover{\n"
"\n"
"    background-color: rgb(40, 40, 40);\n"
"}")
        self.saveimgDunas.setObjectName("saveimgDunas")
        self.horizontalLayout_76.addWidget(self.saveimgDunas)
        self.verticalLayout_2.addLayout(self.horizontalLayout_76)
        self.verticalLayout_13.addWidget(self.groupBox_29)
        self.verticalLayout_12.addWidget(self.groupBox_27)
        self.tabWidget.addTab(self.Drone, "")
        self.about = QtWidgets.QWidget()
        self.about.setObjectName("about")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.about)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.groupBox_5 = QtWidgets.QGroupBox(self.about)
        self.groupBox_5.setTitle("")
        self.groupBox_5.setObjectName("groupBox_5")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.groupBox_5)
        self.verticalLayout_7.setContentsMargins(-1, 0, 0, 0)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.aboutTitle = QtWidgets.QLabel(self.groupBox_5)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.aboutTitle.sizePolicy().hasHeightForWidth())
        self.aboutTitle.setSizePolicy(sizePolicy)
        self.aboutTitle.setStyleSheet("color:rgb(255, 255, 255);\n"
"font: 75 30pt \"Microsoft YaHei\";")
        self.aboutTitle.setObjectName("aboutTitle")
        self.verticalLayout_7.addWidget(self.aboutTitle)
        self.infoAbout = QtWidgets.QLabel(self.groupBox_5)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.infoAbout.sizePolicy().hasHeightForWidth())
        self.infoAbout.setSizePolicy(sizePolicy)
        self.infoAbout.setMinimumSize(QtCore.QSize(0, 200))
        self.infoAbout.setStyleSheet("color:rgb(255, 255, 255);\n"
"font: 75 10pt \"Microsoft YaHei\";")
        self.infoAbout.setObjectName("infoAbout")
        self.verticalLayout_7.addWidget(self.infoAbout)
        self.groupBox_3 = QtWidgets.QGroupBox(self.groupBox_5)
        self.groupBox_3.setMaximumSize(QtCore.QSize(16777214, 180))
        self.groupBox_3.setTitle("")
        self.groupBox_3.setObjectName("groupBox_3")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox_3)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem8 = QtWidgets.QSpacerItem(20, 90, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem8)
        self.contact = QtWidgets.QLabel(self.groupBox_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.contact.sizePolicy().hasHeightForWidth())
        self.contact.setSizePolicy(sizePolicy)
        self.contact.setStyleSheet("color: rgb(255, 255, 255);")
        self.contact.setObjectName("contact")
        self.verticalLayout.addWidget(self.contact)
        self.version = QtWidgets.QLabel(self.groupBox_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.version.sizePolicy().hasHeightForWidth())
        self.version.setSizePolicy(sizePolicy)
        self.version.setStyleSheet("color:rgb(255, 255, 255);\n"
"font: 8pt \"MS Shell Dlg 2\";")
        self.version.setObjectName("version")
        self.verticalLayout.addWidget(self.version)
        self.verticalLayout_7.addWidget(self.groupBox_3)
        self.verticalLayout_6.addWidget(self.groupBox_5)
        self.tabWidget.addTab(self.about, "")
        self.horizontalLayout_67.addWidget(self.tabWidget)
        self.verticalLayout_3.addWidget(self.groupBox_2)
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setStyleSheet("border:none")
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem9 = QtWidgets.QSpacerItem(180, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem9)
        self.discard = QtWidgets.QPushButton(self.groupBox)
        self.discard.setMinimumSize(QtCore.QSize(0, 45))
        self.discard.setMaximumSize(QtCore.QSize(500, 16777215))
        self.discard.setSizeIncrement(QtCore.QSize(0, 0))
        self.discard.setStyleSheet("QPushButton{\n"
"color: rgb(255, 255, 255);\n"
"font: 75 12pt \"Microsoft YaHei\";\n"
"border: 1px solid rgb(255, 255, 255);\n"
"\n"
"}\n"
"QPushButton:hover{\n"
"\n"
"    \n"
"    background-color: rgb(45, 45, 45);\n"
"    \n"
"}\n"
"")
        self.discard.setObjectName("discard")
        self.horizontalLayout_4.addWidget(self.discard)
        self.save = QtWidgets.QPushButton(self.groupBox)
        self.save.setMinimumSize(QtCore.QSize(0, 45))
        self.save.setMaximumSize(QtCore.QSize(500, 16777215))
        self.save.setStyleSheet("QPushButton{\n"
"    color: rgb(255, 255, 255);\n"
"    font: 75 12pt \"Microsoft YaHei\";\n"
"    border: 1px solid rgb(255, 255, 255);\n"
"}\n"
"QPushButton:hover{\n"
"\n"
"    background-color: rgb(45, 45, 45);\n"
"    \n"
"}\n"
"")
        self.save.setObjectName("save")
        self.horizontalLayout_4.addWidget(self.save)
        spacerItem10 = QtWidgets.QSpacerItem(180, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem10)
        self.verticalLayout_3.addWidget(self.groupBox)

        self.retranslateUi(Dialog)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Settings"))
        self.label_5.setText(_translate("Dialog", "Settings"))
        self.label_62.setText(_translate("Dialog", "Register Drone"))
        self.registerDroneButton.setText(_translate("Dialog", "Register"))
        self.label_60.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">Mission Creator</span></p></body></html>"))
        self.missionCreator.setText(_translate("Dialog", "Create"))
        self.label_67.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">Charge Battery:</span></p></body></html>"))
        self.chargeButton.setText(_translate("Dialog", "Charge"))
        self.label_69.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">New User:</span></p></body></html>"))
        self.createUserButton.setText(_translate("Dialog", "Create"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.general), _translate("Dialog", "General"))
        self.label_65.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">Drone</span></p></body></html>"))
        self.label_61.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">Velocity:</span></p></body></html>"))
        self.adjustVelocity.setItemText(0, _translate("Dialog", "Fast"))
        self.adjustVelocity.setItemText(1, _translate("Dialog", "Normal"))
        self.label_63.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">Camera </span></p></body></html>"))
        self.label_41.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">Image format:</span></p></body></html>"))
        self.imgFormatDunas.setItemText(0, _translate("Dialog", "JPEG"))
        self.imgFormatDunas.setItemText(1, _translate("Dialog", "PNG"))
        self.label_43.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">Video resolution:</span></p></body></html>"))
        self.videoResDunas.setItemText(0, _translate("Dialog", "HD"))
        self.videoResDunas.setItemText(1, _translate("Dialog", "Full HD"))
        self.videoResDunas.setItemText(2, _translate("Dialog", "2K"))
        self.videoResDunas.setItemText(3, _translate("Dialog", "UHD"))
        self.label_45.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:12pt;\">Save folder:</span></p></body></html>"))
        self.saveimgDunas.setText(_translate("Dialog", "Path"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.Drone), _translate("Dialog", "Drone"))
        self.aboutTitle.setText(_translate("Dialog", "About "))
        self.infoAbout.setText(_translate("Dialog", "<html><head/><body><p align=\"justify\">Es un hecho establecido hace demasiado tiempo que un lector se </p><p align=\"justify\">distraerá con el contenido del texto de un sitio mientras que mira </p><p align=\"justify\">su diseño. El punto de usar Lorem Ipsum es que tiene una </p><p align=\"justify\">distribución más o menos normal de las letras, al contrario de usar </p><p align=\"justify\">textos como por ejemplo &quot;Contenido aquí, contenido aquí&quot;.</p></body></html>"))
        self.contact.setText(_translate("Dialog", "Contact us: contact@primal.com"))
        self.version.setText(_translate("Dialog", "Version 2.0"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.about), _translate("Dialog", "About"))
        self.discard.setText(_translate("Dialog", "Discard"))
        self.save.setText(_translate("Dialog", "Save"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())
