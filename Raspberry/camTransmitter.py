from os import system, name
from time import sleep
from vidgear.gears import NetGear, VideoGear
import cv2


class Stream():
    def __init__(self, ipTS):
        # stream = cv2.VideoCapture(0) #("udp://127.0.0.1:10000")
        self.stream = VideoGear(source=0).start()
        self.options = {'flag': 0, 'copy': False, 'track': False}
        self.server = NetGear(address=ipTS, port='5454', protocol='tcp',
                              pattern=0, logging=True, **self.options)
        self.running = True

    def vidStream(self):
        while self.running:
            self.server.send(self.stream.read())

    def close(self):  # stream.
        self.stream.stop()
        self.server.close()


if __name__ == '__main__':
    pass  # vidStream('10.29.9.37')
