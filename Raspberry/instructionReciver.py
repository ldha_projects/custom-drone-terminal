from smbus import SMBus
import json
from dronekit import connect, LocationGlobal
import socket
import time
from os import system, name
warning = '\033[92m'
endc = '\033[0m'


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


class DronControl():
    def __init__(self):
        self.host = get_ip_address()  # '172.16.0.10' #socket.gethostname() ""
        self.port = 9998  # 12345 #9999/9998
        self.BUFFER_SIZE = 1024
        self.end = False
        self.telemetryData = ''
        self.conectedd = None
        self.vehicle = connect("/dev/serial0", wait_ready=True, baud=57600)
        self.conectedd = True
        self.addr = 0x8  # bus address
        self.bus = SMBus(1)  # indicates /dev/ic2-1
        self.breaking = False

    def sendTelemetry(self):
        telemetry = {}
        res = []
        # GetPosition
        wholeData = str(self.vehicle.location.global_frame).split(":")
        del wholeData[0]
        data = wholeData[0].split(",")
        del data[2]
        for value in data:
            v = value.split("=")
            del v[0]
            res.append(float(v[0]))
        telemetry["lat"] = res[0]
        telemetry["lon"] = res[1]
        print("pos pasada")
        # Get battery
        wholeData = str(self.vehicle.battery).split(":")
        del wholeData[0]
        data = wholeData[0].split(",")
        v = data[0].split("=")
        percentage = 100*(float(v[1])-21)/4
        telemetry["bat"] = percentage
        print("battery pasada")
        # Get velocity
#        wholeData=  str(self.vehicle.velocity).split(":")
#        del wholeData[0]
#        data = wholeData[0].split(",")
#        del data[2]
#        for value in data:
#            v =value.split("=")
#            del v[0]
#            res.append(float(v[0]))
#        telemetry["velx"]=res[0]
#        telemetry["vely"]=res[1]
#        telemetry["velz"]=res[2]
#        print("velocidad pasada")
        print(telemetry)
        tele = "{}:{}:{}".format(
            telemetry['lat'], telemetry['lon'], telemetry['bat'])
        return tele

    def sendI2c(self, instruction):
        converted = []
        for b in str(instruction):  # "111":
            converted.append(ord(b))
        BytesToSend = converted
        self.bus.write_i2c_block_data(self.addr, 0x00, BytesToSend)
        print("Sent the  command." + str(converted))

    def brkThread(self):
        self.breaking = True
        if self.vehicle.channels.overrides["2"] > 1500:
            self.vehicle.channels.overrides["2"] = 1550
            time.sleep(1)
        elif self.vehicle.channels.overrides["2"] < 1500:
            self.vehicle.channels.overrides["2"] = 1450
            time.sleep(1)
        if self.breaking:
            # self.vehicle.channels.overrrides["3"] = 1225
            if self.vehicle.channels.overrides["2"] < 1500:
                self.vehicle.channels.overrides["2"] = 1525
                time.sleep(1)
            elif self.vehicle.channels.overrides["2"] > 1500:
                self.vehicle.channels.overrides["2"] = 1425
                time.sleep(1)
            if self.vehicle.channels.overrides["2"] < 1500:
                self.vehicle.channels.overrides["2"] = 1525
                time.sleep(1)
            elif self.vehicle.channels.overrides["2"] > 1500:
                self.vehicle.channels.overrides["2"] = 1425
                time.sleep(1)
        if self.breaking:
            self.vehicle.channels.overrides["2"] = 1500

    def connectSocket(self):
        #        self.vehicle = connect("/dev/serial0", wait_ready=True, baud=57600)
        #        self.conectedd = True

        skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print(str(self.port))
        skt.bind((self.host, self.port))
        skt.listen()
        conn, addr = skt.accept()
        while True:
            if self.end == True:
                self.end = False
                break
            data = conn.recv(self.BUFFER_SIZE)
            print(str(data))
            if not data:
                break
            else:
                command = data.decode("ascii")
                print(str(command))
                print(command[:4])
                if command == 'TELE':
                    # print("telemetrysssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss")
                    if self.conectedd:
                        self.telemetryData = str(
                            self.sendTelemetry())
                        print(self.telemetryData)
                    else:
                        pass
                elif command == 'DOWN':  # Look down
                    self.sendI2c("100")
                elif command == 'UP':  # Look up
                    self.sendI2c("101")
                elif command == 'FRONT':  # Look front
                    self.sendI2c("101")
                elif command == 'LEFT':  # Look left
                    self.sendI2c("111")
                elif command == 'RIGHT':  # Look right
                    self.sendI2c("110")
                elif command == 'GSTOP':  # Stop Turn
                    self.sendI2c("001")
                # Aqui Cambien el 1225 para que incrementar o disminuir las RPM al avanzar
                elif command[:4] == "GFWD":
                    self.breaking = False
                    self.vehicle.channels.overrides['3'] = 1225
                    self.aVeloc = int(command[4:])
                    # 1600
                    self.vehicle.channels.overrides["2"] = self.aVeloc
                # Aqui cambien el 1225 para incrementar o disminuir las RPM al retroceder
                elif command[:4] == "GBCK":
                    self.breaking = False
                    self.vehicle.channels.overrides['3'] = 1225
                    self.aVeloc = int(command[4:])
                    # 1375
                    self.vehicle.channels.overrides["2"] = self.aVeloc
                elif command == "DSTOP":
                    self.vehicle.channels.overrides["2"] = 1500
                    self.vehicle.channels.overrides["3"] = 990
                elif command == "BRKE":
                    # self.vehicle.channels.overrides['3'] = 1125
                    # self.vehicle.channels.overrides['2'] = 1500
                    self.brkThread()
                elif command == "TOFF":
                    self.vehicle.channels.overrides["3"] = 1000
                    self.vehicle.channels.overrides["2"] = 1500
                    self.vehicle.armed = True
                    # 1025
                    self.vehicle.channels.overrides["3"] = 1100
                elif command == "SFOF":
                    pass
                elif command == "SFON":
                    pass
                elif command == "RDYD":
                    self.vehicle.channels.overrides["2"] = 1500
                    self.vehicle.channels.overrides["3"] = 1000
                elif command == 'END':
                    self.end = True
                    break
            if command == 'TELE':
                conn.send(self.telemetryData.encode())
            else:
                conn.send(data)
        conn.close()
        skt.close()


def main():
    R = DronControl()
    R.connectSocket()


# if __name__ == "__main__":
#    main()
