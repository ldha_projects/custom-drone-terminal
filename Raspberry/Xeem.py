from goprocam import GoProCamera, constants
import socket
import threading
import camTransmitter
from instructionReciver import DronControl
from os import system, name


def realIP(alledIP):
    if not len(alledIP.split('.')) == 4:
        return False
    y = alledIP.split('.')
    for z in y:
        if len(z) > 1:
            if str(z[0]) == "0":
                return False
        if int(z) not in range(256):
            return False
    return True


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    print(s.getsockname()[0])
    return s.getsockname()[0]

#    print(socket.gethostbyname(socket.gethostname()))
#    return socket.gethostbyname(socket.gethostname())


class Xeem():
    def __init__(self):
        self.host = get_ip_address()
        self.port = 12345
        self.drone = DronControl()
        self.skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.skt.bind((self.host, self.port))

    def start(self):

        self.buffer_size = 1024
        self.running = False
        self.ipToStream = ""
        self.skt.listen()
        self.conn, addr = self.skt.accept()
        self.initialConnection()

    def initialConnection(self):
        try:
            data = self.conn.recv(self.buffer_size)
            message = data.decode('ascii')
            if not data:
                pass
            else:
                if realIP(message):
                    self.conn.send(data)
                    # conn.close()
                    self.ipToStream = message
                    self.running = True
                    self.gimbal_Thread = threading.Thread(
                        target=self.gimbal_Reciver)
                    self.cam_Thread = threading.Thread(
                        target=self.cam_Transmitter)
                    self.gimbal_Thread.start()
                    self.cam_Thread.start()
                    # Waiting for message to close connection
                    data = self.conn.recv(self.buffer_size)
                    message = data.decode('ascii')
                    print(message, "message")
                    # if message == 'END':
                    self.running = False
                    # conn.send(data)
                    self.gimbal_Thread.join()
                    self.cam_Thread.join()
                else:
                    pass
            self.conn.close()
            self.skt.close()
        except:
            pass

    def gimbal_Reciver(self):
        while self.running == True:
            self.drone.connectSocket()
        self.camera.running = False
        print('end gimbalReciver')

    def cam_Transmitter(self):
        self.camera = camTransmitter.Stream(self.ipToStream)
        self.camera.vidStream()
        self.camera.close()


def main():
    xeem = Xeem()
    while True:
        xeem.start()


if __name__ == "__main__":
    main()
