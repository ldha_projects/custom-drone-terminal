# Ardupix4

_Control station to command drones using a Pixhawk 4 with Ardupilot, a raspberry 4 comunicating with sockets to a Server using Python_

### Requirements

_Required Libraries_

```
PyQt5
numpy as np
cv2(OpenCv)
Zlib
pickle
struct
vidgear
pillow
```

### Installation
**Install PyQt5 library**
```
pip install PyQt5
```

**Install cv2(OpenCv)**
```
pip install opencv-python
```

**Install Zlib**
```
pip install 
```

**Install vidgear**
```
pip instal vidgear
```

**Install pillow**
```
pip instal Pillow
```

**Install pyinstaller**
```
pip install pyinstaller
```
**Download current repository**
 ```
 git clone https://github.com/NE0RI0/Ardupix4.git
 ```
 _Now open a terminal, and run this command on the directory that you downloaded the repositotry_
 ```
 pyinstaller -F -w .\main.py --add-binary 'C:\Program Files (x86)\Python38-32\Lib\site-packages\cv2\opencv_videoio_ffmpeg420.dll;.'
 ```
 _Now you have to copy **settings.json**, **users.json**, **missions.json** and the **icons** directory into **dist** folder_
 
 
 _Now you can run the program✨🙌_

### Documentation
_The program divides in multiple modules_

**1. main.py(Main class)**
    **- Class that contains the main functionality of ui**
     -  update_message(username, password, accesLevel),
     -  openLogIn(),
     -  LogOut(),
     -  connectDrone(),
     -  addDroneList(),
     -  addItemQCombo(),
     -  AutoMode(),
     -  returnToHome(),
     -  rthButtonFunction(),
     -  missionExcecuter(),
     -  pausarRutina(),
     -  stopRoutine(),
     -  viewCam(),
     -  controlTimer(),
     -  recordCam(recimg),
     -  pauserecording(),
     -  takepicture(),
     -  timerUp(),
     -  updateStatus(),
     -  goFoward(),
     -  goBack(),
     -  goUp(),
     -  goDown(),
     -  terminateGimbal(),
     -  turnRight(),
     -  turnLefth(),
     -  stopTurn(),
     -  stop(),
     -  startTakeOff(),
     -  startLanding(),
     -  estop(),
     -  returnToHome(),
     -  updateSettingsData(),
     -  askTelemetry(),
     -  getTele(),
     -  closeEvent()
     
     
**2. loginWidget.py(logInWindow class)**
   **- A widget to grant access to main window**
     -  set_messages(username, password),
     -  closeLog(),
     -  on_submit()
     
     
**3. createMission.py(MissionCreatorDrone class)**
   **- A widget to create missions for the drone**
     -  newondInstruction(),
     -  newofdInstruction(),
     -  newrthInstruction(),
     -  newstnInstruction(),
     -  newtltInstruction(),
     -  newtrtInstruction(),
     -  newgupInstruction(),
     -  newgdnInstruction(),
     -  newoncInstruction(),
     -  newofcInstruction(),
     -  newstrInstruction(),
     -  newpsaInstruction(),
     -  newphtInstruction(),
     -  newendInstruction(),
     -  newedtInstruction(),
     -  dltInstruction(),
     -  saveInstruction(),
     -  editMission()
     
     
**4. showDrones.py(ShowDrones class)**
   **- A widget add registeref drones to a QList**
     -  UpdateList(),
     -  Delete()
     
     
**5. ardupix4.py(Ardupix4 class)**
   **- The class to create a drone object with all it's commands**
     -  ardustart(),
     -  goForward(),
     -  goBack(),
     -  stop(),
     -  break(),
     -  takeOff(),
     -  safetyOff(),
     -  safetyOn(),
     -  readyDrone(),
     -  endConnection(),
     -  upInstruction(),
     -  downInstruction(),
     -  rightInstruction(),
     -  leftInstruction(),
     -  stopInstruction(),
     -  frontInstruction(),
     -  endInstruction(),
     -  askTelemetry(),
     -  sendDrone(comm_drone),
     -  sendDrone(comm_drone),
     
    
